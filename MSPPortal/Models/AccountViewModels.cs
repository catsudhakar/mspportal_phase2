﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Compare = System.ComponentModel.DataAnnotations.CompareAttribute;

namespace MSPPortal.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [EmailAddress(ErrorMessage = "Enter a valid E-mail address")]
        [Display(Name = "Email")]
        [Remote("CheckExistingEmail", "Account", ErrorMessage = "Email already exists!", AdditionalFields = "InitialEmail")]
        public string Email { get; set; }

        public string Password { get; set; }

       

        [Required]
        [StringLength(100, ErrorMessage = "The Name should be with in 50 characters lenth")]
        [Display(Name = "Name")]
        public string Name { get; set; }



        [Required]
        [Display(Name = "Mobile Number")]
        [RegularExpression(@"^(966\d{9})$", ErrorMessage = "Please enter valid mobile number in the format 9665xxxxxxxx.")]
        [Remote("CheckExistingMobile", "Account", ErrorMessage = "Mobile No already exists!",AdditionalFields = "InitialMobileNumber")]
        public string MobileNumber { get; set; }

        public string Id { get; set; }

        [Required]
        [Display(Name = "Role Name")]
        public string RoleName { get; set; }

        public List<RoleNames> Roles { get; set; }
        //public ICollection<SelectListItem> Roles { get; set; }


    }

    public class RoleNames
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

    public class StaffViewModel
    {

        public string Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string MobileNumber { get; set; }
        public string RoleName { get; set; }
        public bool loggedUser { get; set; }

    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        [Remote("CheckExistingEmail1", "Account", ErrorMessage = "No user with given email!")]
        public string Email { get; set; }
    }
}
