﻿var commonModule = angular.module('share',
    [
      'ngAnimate',        // animations
        'ngRoute',          // routing
        'ngSanitize',       // sanitizes html bindings (ex: sidebar.js)
        'ngMaterial',
        // Custom modules 
        'common',           // common functions, logger, spinner
        //'common.bootstrap', // bootstrap dialog wrapper functions
        // 3rd Party Modules        
       //'angular-loading-bar',
        'ui.bootstrap',      // ui-bootstrap (ex: carousel, pagination, dialog),
        //'data-table',
        //'md.data.table',
        //'ngFileUpload',
        //'ngImgCrop',
        //'pascalprecht.translate',

        'ui.bootstrap.datetimepicker',
        'ui.dateTimeInput'

    ]);

commonModule.directive('animateOnLoad', ['$animateCss', function ($animateCss) {
    return {
        'link': function (scope, element) {
            $animateCss(element, {
                'event': 'enter',
                structural: true
            }).start();
        }
    };
}]);

commonModule.controller('confirmationDialogCtrl', function ($scope, $modalInstance) {
    $scope.ok = function () {
        $modalInstance.close();
    };
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});
commonModule.controller('loginDialog', function ($scope, $mdDialog) {
    $scope.cancel = function () {
        $mdDialog.cancel();
    };
    $scope.login = function () {
        window.location.href = "/Account/Login";
    }

});

commonModule.value("config", {
    events: {
        spinnerToggle: 'spinner.toggle'
    }
});
commonModule.directive("scroll", function ($window, $document) {
    return function (scope, element, attrs) {
        angular.element($window).bind("scroll", function () {
            if ($window.pageYOffset === $document.height() - $window.innerHeight) {
                scope.$apply(attrs.scroll);
            }
        });

    };
});
commonModule.controller('ToastCtrl', function ($scope, $mdToast) {
    $scope.closeToast = function () {
        $mdToast.hide();
    };
});
var mainModule = angular.module('main', ['share']), hub = $.connection.messagesHub;

commonModule.factory('viewModelHelper', function ($http, $q, $window, $location) { return MyApp.viewModelHelper($http, $q, $window, $location); });
commonModule.factory('validator', function () { return valJs.validator(); });
commonModule.factory('toasterService', function ($mdToast, $mdDialog) {
    var self = this;
    self.displaySuccessToaster = function (message) {
        $mdToast.show({
            controller: 'ToastCtrl',
            template: '<md-toast class="md-toast"><span flex>' + message + '</span><md-button ng-click="closeToast()">Close</md-button></md-toast>',
            hideDelay: 6000,
            position: 'bottom right'
        });
    }

    self.displayWarningToaster = function (message) {
        $mdToast.show({
            controller: 'ToastCtrl',
            template: '<md-toast class="md-toast error"><span flex>' + message + '</span><md-button  ng-click="closeToast()">Close</md-button></md-toast>',
            hideDelay: 6000,
            position: 'bottom right'
        });
    }
    self.displaySuccessDialog = function (message) {
        $mdDialog.show(
          $mdDialog.alert()
           .clickOutsideToClose(true)
           .textContent(message)
           .ariaLabel('Alert Dialog Demo')
           .ok('Ok')
        );
    }
    return this;
});
mainModule.controller("indexViewModel",
function ($scope, $http, $q, $routeParams, $window, $location, viewModelHelper, $mdDialog, $rootScope, config, $mdSidenav, $route, $sce) {

    var self = this;
    window.MyApp.$rootScope = $rootScope;
    var basePath = "/";
    $scope.grades = [];
    $scope.gates = [];
    $scope.sections = [];
    $scope.selectedGrade = '';
    $scope.selectedSection = '';

    $scope.selectedNotificationGrade = '';
    $scope.selectedNotificationSection = '';

    $.connection.hub.start().done(function () {

    });

    var focused = true;
   

    window.onfocus = window.onblur = function (e) {
        focused = (e || event).type === "focus";
    }

    $scope.isSubmitWaiting = false;
    //if ($window.location.pathname === '/dashboard') {
    $scope.pageTitle = "MiSK Schools Page - Portal";
    //} else if ($window.location.pathname === '/staffDashboard') {
    //    $scope.pageTitle = "MiSK Schools Page - Dashboard";
    //}

    self.languages = [{
        name: "en",
        displayName: "English"
    }, {
        name: "es",
        displayName: "Spanish"
    }
    ];

    var language = sessionStorage.getItem("selectedLanguage");
    if (!language) {
        self.selectedLanguage = "en";
    }
    else {
        self.selectedLanguage = language;
    }

    self.languageChanged = function () {
        // alert(self.selectedLanguage);
        $translate.use(self.selectedLanguage);
        sessionStorage.setItem("selectedLanguage", self.selectedLanguage);
        $rootScope.$broadcast("languageChanged", self.selectedLanguage);
    }

    $rootScope.$on("AddedNewNofication", function (event, data) {
        $scope.notifications.splice(0, 0, data);
        $scope.$apply();
    });


    $scope.updateNotifications= function () {
        // alert($scope.selectedNotificationGrade);
        if ($scope.selectedNotificationGrade == '' && $scope.selectedNotificationSection == '') {
            getNotifications();
        }
        else if ($scope.selectedNotificationGrade != '' && $scope.selectedNotificationSection == '') {
            $scope.getSectionByGradeId($scope.selectedNotificationGrade);
        }
        else {
            getNotifications($scope.selectedNotificationGrade, $scope.selectedNotificationSection);
        }
        $scope.$apply();
    }

    $scope.encodeHtml = function (inputString) {
        return $sce.trustAsHtml(inputString);
    };

    activate();

    function activate() {
        if (window.location.pathname == "/" || window.location.pathname.indexOf('Account') > -1) {

        }
        else {
            getNotifications();
            setSendReminder();

            $http({
                method: 'POST',
                url: basePath + 'Home/GetData',

            }).success(function (res) {
                if (res != null) {
                    $scope.grades = res.Grades;
                    $scope.gates = res.Gates;
                    $scope.sections = res.Sections;
                    if (res.Grades.length > 0) {
                        $scope.selectedGrade = res.Grades[0].GradeId;
                    }
                }
            }).error(function (err) {
                console.log(err);
            });
        }

    }

    $scope.getSectionByGradeId = function (GradeId) {
        $scope.selectedSection = '';

        $scope.selectedNotificationSection = '';
        $scope.notifications = [];
        $scope.sections = [];

        $http({
            method: 'POST',
            url: basePath + 'Home/GetSectionsAndNotificationsByGradeId',
            data: {
                Id: JSON.stringify(GradeId)
            },
        }).success(function (data) {
            $scope.sections = data.Item1;
            $scope.notifications = data.Item2;
        }).error(function (err) {
            console.log(err);
        });


    }

    $scope.getNotificationsBySection = function (GradeId, SectionId) {
        $scope.notifications = [];

        $http({
            method: 'POST',
            url: basePath + 'Home/GetNotifications',
            data: {
                GradeId: JSON.stringify(GradeId), SectionId: JSON.stringify(SectionId)
            },

        }).success(function (data) {
            $scope.notifications = data;
        }).error(function (err) {
            console.log(err);
        });



    }

    function getNotifications(GradeId, SectionId) {
        $scope.notifications = [];

        $http({
            method: 'POST',
            url: basePath + 'Home/GetNotifications',
            data: {
                GradeId: JSON.stringify(GradeId), SectionId: JSON.stringify(SectionId)
            },

        }).success(function (data) {
            $scope.notifications = data;
        }).error(function (err) {
            console.log(err);
        });

    }


    function setSendReminder() {

        setInterval(function () { sendReminder(); }, 600000);
    }

    function sendReminder() {
       

        $http({
            method: 'POST',
            url: basePath + 'Home/SendReminder',
            //data: { GradeId: JSON.stringify(GradeId), SectionId: JSON.stringify(SectionId) },

        }).success(function (data) {
            // $scope.notifications = data;

        }).error(function (err) {
            console.log(err);
        });
        //}

    }




    $scope.isBusy = false;
    $scope.busyMessage = "";
    $scope.spinnerOptions = {
        radius: 40,
        lines: 7,
        length: 0,
        width: 30,
        speed: 1.7,
        corners: 1.0,
        trail: 100,
        color: '#F58A00'
    };

    var events = config.events;
    function toggleSpinner(on, msg) {
        $scope.isBusy = on;
        $scope.busyMessage = msg;
    }

    $rootScope.$on(events.spinnerToggle,
       function (data, param) {
           toggleSpinner(param.show, param.msg);
       });


    $scope.toggleNotifications = buildToggler('right');

    function buildToggler(navID) {

        return function () {

            $mdSidenav(navID)
              .toggle()
              .then(function () {


              });
        }
    }

    $scope.toggleRight = buildToggler('right');
    $scope.isOpenRight = function () {
        return $mdSidenav('right').isOpen();
    };

    $scope.close = function () {

        $mdSidenav('right').close()
          .then(function () {
              $log.debug("close RIGHT is done");
          });
    };

    $scope.redirectToAdminDashboard = function () {
        if ($window.location.pathname === '/dashboard') {
            $window.location.reload();
        }
        else {
            window.location.href = '/dashboard';
        }
    };
    $scope.redirectToStaffDashboard = function () {
        if ($window.location.pathname === '/staffDashboard') {
            $window.location.reload();
        }
        else {
            window.location.href = '/staffDashboard';
        }
    };

    hub.client.updateMessages = function (notificationList) {

        $scope.updateNotifications();
        $rootScope.$broadcast("SignalRUpdateToHome", notificationList);
        angular.forEach(notificationList, function (notification, key) {
            DesktopNotification(notification.Message);
        });
       
    };

    $.connection.hub.start();

    function DesktopNotification(message) {

      
        var nAgt = navigator.userAgent;
       

        var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
        if (!isSafari) {
            if (!("Notification" in window)) {
                alert("This browser does not support desktop notification");
            }

                // Let's check whether notification permissions have already been granted
            else if (Notification.permission === "granted") {

                if (!focused) {
                    console.log('focussed');
                    var notification = new Notification('', {
                        icon: window.location.href.split('/')[0] + '/Content/Images/notification_logo.png',
                        body: message.replace(/<[\/]{0,1}(b)[^><]*>/g, "")
                    });
                    //close the notification automatically after 5 seconds
                    setTimeout(notification.close.bind(notification), 5000);
                }
            }
                // Otherwise, we need to ask the user for permission
            else if (Notification.permission !== 'granted') {
                Notification.requestPermission(function (permission) {
                    // If the user accepts, let's create a notification
                    // console.log(message + 'if');
                    if (permission === "granted") {
                        console.log('grantedbyvenkat');
                        var notification = new Notification('', {
                            icon: window.location.href.split('/')[0] + '/Content/Images/notification_logo.png',
                            body: message.replace(/<[\/]{0,1}(b)[^><]*>/g, "")
                        });
                        //close the notification automatically after 5 seconds
                        setTimeout(notification.close.bind(notification), 5000);
                    }
                });
            }

            // At last, if the user has denied notifications, and you 
            // want to be respectful there is no need to bother them any more.
        }
        Notification.requestPermission().then(function (result) {
            console.log(result);
        });
        function spawnNotification(theBody, theIcon, theTitle) {
            var options = {
                body: theBody,
                icon: theIcon
            }
            var n = new Notification(theTitle, options);

        }
    }  //end desktop notifications

}













);
(function (myApp) {
    var viewModelHelper = function ($http, $q, $window, $location) {

        var self = this;

        self.modelIsValid = true;
        self.modelErrors = [];

        self.resetModelErrors = function () {
            self.modelErrors = [];
            self.modelIsValid = true;
        }

        self.apiGetByParm = function (uri, params, success, failure, always) {
            self.modelIsValid = true;

            $http({
                method: 'GET',
                url: MyApp.rootPath + uri,
                params: params
            })
                .then(function (result) {
                    success(result);
                    if (always != null)
                        always();
                }, function (result) {
                    if (failure != null) {
                        failure(result);
                    }
                    else {
                        var errorMessage = result.status + ':' + result.statusText;
                        if (result.data != null && result.data.Message != null)
                            errorMessage += ' - ' + result.data.Message;
                        self.modelErrors = [errorMessage];
                        self.modelIsValid = false;
                    }
                    if (always != null)
                        always();
                });
        }

        self.apiGet = function (uri, success, failure, always) {
            self.modelIsValid = true;
            $http.get(MyApp.rootPath + uri)
                .then(function (result) {
                    success(result);
                    if (always != null)
                        always();
                }, function (result) {
                    if (failure != null) {
                        failure(result);
                    }
                    else {
                        var errorMessage = result.status + ':' + result.statusText;
                        if (result.data != null && result.data.Message != null)
                            errorMessage += ' - ' + result.data.Message;
                        self.modelErrors = [errorMessage];
                        self.modelIsValid = false;
                    }
                    if (always != null)
                        always();
                });
        }

        self.apiPost = function (uri, data, success, failure, always) {
            self.modelIsValid = true;
            $http.post(MyApp.rootPath + uri, data)
                .then(function (result) {
                    success(result);
                    if (always != null)
                        always();
                }, function (result) {
                    if (failure != null) {
                        failure(result);
                    }
                    else {
                        var errorMessage = result.status + ':' + result.statusText;
                        if (result.data != null && result.data.Message != null)
                            errorMessage += ' - ' + result.data.Message;
                        self.modelErrors = [errorMessage];
                        self.modelIsValid = false;
                    }
                    if (always != null)
                        always();
                });
        }

        self.goBack = function () {
            $window.history.back();
        }

        self.navigateTo = function (path) {

            $location.path(path);
        }

        self.refreshPage = function (path) {
            $window.location.href = MyApp.rootPath + path;
        }

        self.clone = function (obj) {
            return JSON.parse(JSON.stringify(obj))
        }

        return this;
    };
    myApp.viewModelHelper = viewModelHelper;
}(window.MyApp));
