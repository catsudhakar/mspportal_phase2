﻿(function () {
    'use strict';

    angular
        .module('staff')
        .controller('staffList', staffList);

    staffList.$inject = ['$scope', '$http','$mdDialog', 'toasterService'];

    function staffList($scope, $http,$mdDialog, toasterService) {

        $scope.title = 'Observer List';
        $scope.staffs = [];
        $scope.CurrentPage = 0;
        $scope.pageSize = 10;
        var basePath = "/";
        activate();
       

        function activate() {

            $http({
                method: 'POST',
                url: basePath + 'Home/GetStaff',

            }).success(function (res) {
                if (res !== null) {
                    $scope.staffs = res;
                    var hdnSuccMessage = document.getElementById("hdnSuccessMessage");
                    if (hdnSuccMessage.value !== '') {
                        setTimeout(function () { toasterService.displaySuccessDialog(hdnSuccMessage.value); }, 300);
                       
                    }
                }
            }).error(function (err) {
                console.log(err);
            });

           
        }

        $scope.numberOfPages = function () {
            return Math.ceil($scope.staffs.length / $scope.pageSize);
        }

        $scope.register = function (Id) {
            if (Id)
                window.location.href = '/Account/Register/' + Id;
            else
                window.location.href = '/Account/Register/';
        }

        $scope.delete = function (Id,name,index) {
            var confirm = $mdDialog.confirm()
                 .title('Do you want to delete "' + name+'" ?')
                 .ariaLabel('Do you want to proceed?')
                 .ok('Ok')
                 .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {

                $http({
                    method: 'POST',
                    url: basePath + 'Account/Delete',
                    data:{'Id': Id,}
                }).success(function (res) {
                    if (res !== null) {
                        $scope.staffs.splice(index, 1);
                        toasterService.displaySuccessDialog('Observer deleted successfully');
                    }
                }).error(function (err) {
                    console.log(err);
                });

            }, function () {

                 // window.MyApp.$rootScope.$broadcast("ToasterWarningUpdate",'Cancel');

            });
        }

    }
})();

angular.module('staff').filter('startFrom', function () {
    return function (input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});
