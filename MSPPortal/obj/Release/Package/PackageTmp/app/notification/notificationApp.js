﻿var notificationModule = angular.module('notification', ['share'])
    .config(function ($routeProvider, $locationProvider) {
        $routeProvider.when('/notificationHistory',
            {
                templateUrl: '/app/notification/notificationsList.html'

            });
        $routeProvider.when('/notification/studentStatusReport',
            {
                templateUrl: '/app/notification/studentStatusReport.html'

        });
      
        $routeProvider.otherwise({ redirectTo: '/notificationHistory' });
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    });





(function (myApp) {
    var notificationService = function ($rootScope, $http, $q, $location, viewModelHelper) {

        var self = this;

        // self.customerId = 0;

        return this;
    };
    myApp.notificationService = notificationService;
}(window.MyApp));


