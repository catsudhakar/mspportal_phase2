﻿(function () {
    'use strict';

    angular
        .module('home')
        .controller('addBusController', addBus);

    addBus.$inject = ['$scope','$mdDialog']; 

    function addBus($scope, $mdDialog) {
        $scope.title = 'addBus';

        activate();

        function activate() { }

        $scope.cancel = function () {
            $mdDialog.cancel();
        };
    }
})();
