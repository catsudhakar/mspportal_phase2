﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MSPPortal.ViewModel
{
    public class GradeViewModel
    {
        public int GradeId { get; set; }

        public string GradeName { get; set; }
                
    }

    public class GateViewModel
    {
        public int GateId { get; set; }

        public string GateName { get; set; }

    }

    public class SectionViewModel
    {
        public int SectionId { get; set; }

        public string GradeName { get; set; }

        public string SectionName { get; set; }

    }

    public class NotificationViewModel
    {

        public string Message { get; set; }

        public int Id { get; set; }

        public string StudentName { get; set; }

        public string NotificationTime { get; set; }

        public string NotificationDate { get; set; }

        public string Grade { get; set; }

        public string Section { get; set; }

        public string GateName { get; set; }

        public string Status { get; set; }

        public Boolean IsNotified { get; set; }

        public string StudentId { get; set; }

    }
}