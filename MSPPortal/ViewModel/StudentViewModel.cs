﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MSPPortal.ViewModel
{
    public class StudentViewModel
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public DateTime DOB { get; set; }

        public string Grade { get; set; }

        public string Section { get; set; }

        public string SectionName { get; set; }

        public string GuardianName { get; set; }

        public Guid? GuardianId { get; set; }

        public int Status { get; set; }

        public DateTime CurrentDate { get; set; }

        public string StatusName { get; set; }

        public string WaitingTime { get; set; }

        public string QueueTime { get; set; }

        public string DismissTime { get; set; }

        public string PickedUpTime { get; set; }

        public string GateName { get; set; }

        public string FullName { get; set; }

        public DateTime? UpdatedTime { get; set; }

        public bool IsGuardianNotRegisterd { get; set; }
    }

    public class GuardianViewModel
    {
        public Guid Id { get; set; }
        public string FirtName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string DeviceId { get; set; }
        public string MobileOtp { get; set; }
    }

    public class NotificationListViewModel
    {
        public Guid StudentId { get; set; }
        public string StudentName { get; set; }
        public Guid GuardianId { get; set; }
        public string GuardianName { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public string NotificationDate { get; set; }
        public string NotificationTime { get; set; }
        public string NotificationFrom { get; set; }
        public string GradeId { get; set; }
        public string GradeName { get; set; }
        public string SectionId { get; set; }
        public string SectionName { get; set; }
        public Nullable<System.DateTime> NotificationCurrentDate { get; set; }
    }

    public class StudentStatusListViewModel
    {
        public string StudentId { get; set; }
        public string StudentName { get; set; }
        public string GuardianName { get; set; }
        public string DeviceId { get; set; }
        public string PhoneNumber { get; set; }
        public string GradeId { get; set; }
        public string GradeName { get; set; }
        public string SectionId { get; set; }
        public string SectionName { get; set; }
        public System.DateTime CurrentDate { get; set; }
        public string CurrentDateString { get; set; }
        public string WaitingTime { get; set; }
        public string QueueTime { get; set; }
        public string DismissTime { get; set; }
        public string PickedUpTime { get; set; }

        public DateTime WaitingDateTime { get; set; }
        public DateTime QueueDateTime { get; set; }
        public DateTime DismissDateTime { get; set; }
        public DateTime PickedUpDateTime { get; set; }


        public string StatusName { get; set; }
        public int? Status { get; set; }
        public Nullable<System.DateTime> LastUpdatedTime { get; set; }
    }
    public class StudentModel
    {
        public string Id { get; set; }

        public string FirstName { get; set; }

    }

    public class StatusViewModel
    {
        public int Id { get; set; }
        public System.Guid StudentId { get; set; }
        public System.DateTime CurrentDate { get; set; }
        public Nullable<System.TimeSpan> WaitingTime { get; set; }
        public Nullable<System.TimeSpan> QueueTime { get; set; }
        public Nullable<System.TimeSpan> DismissTime { get; set; }
        public Nullable<System.TimeSpan> ReminderTime { get; set; }
        public Nullable<System.TimeSpan> PickedUpTime { get; set; }
        public Nullable<int> GateId { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<System.DateTime> UpdatedTime { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Grade { get; set; }
        public string SectionId { get; set; }
    }
}