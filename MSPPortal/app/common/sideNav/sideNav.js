﻿angular.module('main').controller("sidenavController",
    function ($scope, $timeout, $mdSidenav, $mdDialog, $log, $http, viewModelHelper) {
    $scope.progressMode = "indeterminate";
    $scope.fabSelectionMode = "md-fling";
    $scope.isOpen = false;
    $scope.selectedMode = 'md-fling';
    $scope.selectedDirection = 'up';

    $scope.toggleSidebar = function (sectionId, sectionName) {
        sessionStorage.setItem("sectionId", sectionId);
        sessionStorage.setItem("sectionName", sectionName);
        getCategories(sectionId);
        return $mdSidenav('right').toggle();

    };

    $scope.close = function () {
        $mdSidenav('right').close();
    };
    function getCategories(sectionId) {

        viewModelHelper.apiPost('Home/GetCategoriesBySectionId', '"' + sectionId + '"',
        function (res) {
            $scope.sectionCategories = res.data;
            $scope.progressMode = "";
            
        },
        function (err) {
             console.log(err);
        });
    }
    $scope.addPost = function (ev, categoryId) {
        sessionStorage.setItem("categoryId", categoryId);
        $mdSidenav('right').close();
        $mdDialog.show({
            templateUrl: '/app/common/addPost/addPost.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true
        })
        .then(function (res) {

        }, function () {

        });
    };

});