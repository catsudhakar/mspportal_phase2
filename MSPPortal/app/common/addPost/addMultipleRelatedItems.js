﻿(function () {
    'use strict';

    angular
        .module('main')
        .controller('addMultipleRelatedItems', addMultipleRelatedItems);

    addMultipleRelatedItems.$inject = ['$scope', '$http', '$mdDialog', '$mdBottomSheet','$rootScope','viewModelHelper','$translate'];

    function addMultipleRelatedItems($scope, $http, $mdDialog, $mdBottomSheet, $rootScope,viewModelHelper,$translate) {
      
       var postId = sessionStorage.getItem("postId");
       var sectionId = sessionStorage.getItem("sectionId");
       $scope.progressMode = "indeterminate";
       
        viewModelHelper.apiPost('Home/GetPostsBySection', [postId,sectionId],
        function (res) {
           
               $scope.relatedPosts = res.data;
               $scope.progressMode = "";
        },
        function (err) {
             console.log(err);
        });
        $scope.cancel = function () {
            $mdDialog.cancel();
        };
        $scope.closeBottomSheet=function()
        {
            $mdBottomSheet.hide();
        }
        $scope.saveRelatedItem=function(postId)
        {
            //$mdBottomSheet.hide({ id: $scope.postIdList });
            //$translate(['Layout.AreYouSure', 'Layout.OK', 'Layout.Cancel']).then(function (translation) {
            //    $scope.message = translation['Layout.AreYouSure'];
            //    $scope.ok = translation['Layout.OK'];
            //    $scope.cancel = translation['Layout.Cancel'];

            //    var confirm = $mdDialog.confirm()
            //            .title($scope.message)
            //            .ariaLabel('Related Item')
            //            .ok($scope.ok)
            //            .cancel($scope.cancel);
            //    $mdDialog.show(confirm).then(function () {
                    $mdBottomSheet.hide({ id: postId });

            //    }, function () {
            //        $scope.status = 'You decided to keep your debt.';
            //    });
            //});
        }
    }
})();
