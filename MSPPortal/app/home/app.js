﻿var homeModule = angular.module('home', ['share'])
    .config(function ($routeProvider, $locationProvider) {
        $routeProvider.when('/dashboard',
            {
                templateUrl: '/app/home/index.html'

            });
        $routeProvider.when('/Admin/BusDetails',
            {
                templateUrl: '/app/home/busDetails.html'

            });
        $routeProvider.when('/Admin/RouteDetails',
            {
                templateUrl: '/app/home/routeDetails.html'

            });
        $routeProvider.when('/staffList',
            {
                templateUrl: '/app/home/staffList.html'

            });

        $routeProvider.otherwise({ redirectTo: '/dashboard' });
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    });





(function (myApp) {
    var homeService = function ($rootScope, $http, $q, $location, viewModelHelper) {

        var self = this;

        // self.customerId = 0;

        return this;
    };
    myApp.homeService = homeService;
}(window.MyApp));


