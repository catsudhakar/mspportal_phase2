﻿(function () {
    'use strict';

    angular
        .module('home')
        .controller('pickupPointsCtrl', pickupPoints);

    pickupPoints.$inject = ['$scope']; 

    function pickupPoints($scope) {
        $scope.title = 'pickupPoints';

        activate();

        function activate() { }
    }
})();
