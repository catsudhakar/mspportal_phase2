﻿(function () {
    'use strict';
    var app = angular.module('home');

    app.controller('index', ['$scope', '$http', '$filter', '$mdToast', 'spinner', 'toasterService', '$mdDialog', index]);

    function index($scope, $http, $filter, $mdToast, spinner, toasterService, $mdDialog) {



        var vm = this;
        vm.students = [];
        vm.waitingStudents = [];
        vm.queueStudents = [];
        vm.pickupStudents = [];
        vm.dismissStudents = [];
        $scope.gateValidation = [];

        $scope.chkwaiting = false;

        $scope.grades = [];
        $scope.gates = [];
        $scope.sections = [];
        $scope.selectedGrade = '';
        $scope.selectedSection = '';
        $scope.selectedTopGate = '';
        $scope.selectedGate = '';
        $scope.isSubmitWaiting = false;
        $scope.isSectionSelected = false;


        $scope.hideLoader = true;
        var basePath = "/";

        activate();



        function activate() {

            $scope.progressMode = "";
            $scope.hideLoader = true;

            $http({
                method: 'POST',
                url: basePath + 'Home/GetData',

            }).success(function (res) {
                if (res !== null) {
                    $scope.grades = res.Grades;
                    $scope.gates = res.Gates;
                    $scope.sections = res.Sections;
                    if (res.Grades.length > 0) {
                        $scope.selectedGrade = res.Grades[0].GradeId;
                    }

                    // DesktopNotificationTime();

                }
            }).error(function (err) {
                console.log(err);
            });


        }

        function DesktopNotificationTime() {
            setInterval(function () { DesktopNotification('Hello'); }, 60000);
        }

        $scope.getSectionByGradeId = function (GradeId) {
            $scope.selectedSection = '';
            $scope.sections = [];
            $http({
                method: 'POST',
                url: basePath + 'Home/GetSectionByGradeId',
                data: { Id: JSON.stringify(GradeId) },
            }).success(function (data) {
                $scope.sections = data;
            }).error(function (err) {
                console.log(err);
            });



        }

        $scope.getStudents = function (gradeId, sectionId) {

            $scope.chkwaiting = false;
            $scope.chkinqueue = false;
            vm.waitingStudents = [];
            vm.queueStudents = [];
            vm.pickupStudents = [];
            $scope.hideLoader = true;
            $scope.progressMode = "";
            $scope.isSectionSelected = true;

            $http({
                method: 'POST',
                url: basePath + 'Home/GetStudents',
                data: { GradeId: JSON.stringify(gradeId), SecId: JSON.stringify(sectionId) },
            }).success(function (data) {
                vm.waitingStudents = data.Item1;
                vm.queueStudents = data.Item2;
                vm.pickupStudents = data.Item3;
                vm.dismissStudents = data.Item4;

                setNanoScroll();
                // var even = _.find(vm.waitingStudents, function (num) { return num % 2 == 0; });


            }).error(function (err) {
                console.log(err);
            });



        }

        $scope.sendWaiting = function (gradeId, sectionId, form) {


            $scope.isSubmitWaiting = true;
            $scope.isSectionSelected = true;
            if (form.$valid && $scope.isSubmitWaiting) {
                $scope.hideLoader = false;
                $scope.progressMode = "indeterminate";
                $http({
                    method: 'POST',
                    url: basePath + 'Home/SendToWaiting',
                    data: { GradeId: JSON.stringify(gradeId), SecId: JSON.stringify(sectionId) },
                }).success(function (data) {
                    vm.waitingStudents = data;
                    angular.forEach(vm.waitingStudents, function (item) {
                        item.Grade = gradeId;
                        item.SectionName = $("#selSection option:selected").text();
                    })

                    setNanoScroll();
                    $scope.hideLoader = true;
                    $scope.progressMode = "";
                }).error(function (err) {
                    console.log(err);
                    $scope.hideLoader = true;
                    $scope.progressMode = "";
                });
            }
            else {
                $scope.hideLoader = true;
                $scope.progressMode = "";
                //toasterService.displayWarningToaster('Please select section');
                //toasterService.displaySuccessDialog('Please select section');
                $scope.isSectionSelected = false;
            }


        }


        $scope.isSendTOQueueSubmited = [];
        $scope.sendToQueue = function (student, gateId, index) {

            // alert(gateId);
            if (gateId !== undefined && student.selected) {

                $scope.hideLoader = false;
                $scope.progressMode = "indeterminate";
              
                $http({
                    method: 'post',
                    url: basePath + 'home/sendtoqueue',
                    data: { student: student, gateid: gateId,sectionId:$scope.selectedSection },
                }).success(function (data) {
                  
                        vm.waitingStudents.splice(index, 1);
                        vm.queueStudents.splice(0, 0, data);
                        angular.forEach(vm.queueStudents, function (item) {
                            item.SectionName = $("#selSection option:selected").text();
                        })
                        setNanoScroll();
                        $scope.hideLoader = true;
                        $scope.progressMode = "";
                        if (data.IsGuardianNotRegisterd==false) {
                            toasterService.displaySuccessDialog('Student ' + student.FirstName + " successfully sent to queue.");
                        }else{
                            toasterService.displaySuccessDialog("There is no notification sent to guardian of "+student.FirstName+" because no guardian is not registered with mobile");
                        }

                }).error(function (err) {
                    console.log(err);
                    $scope.hideLoader = true;
                    $scope.progressMode = "";
                });
            }
            else {
                // $scope.gateValidation[index] = true;
                // toasterService.displayWarningToaster('Please select both student and gate ');
                toasterService.displaySuccessDialog('Please select both student and gate.');
            }


        }

        $scope.sendAllQueue = function (gateId) {



            $scope.selectedWaitingStudents = [];
            if (!$scope.selectedTopGate) {
                $scope.isSendAllQueueSubmited = true;
            }
            else {

               
                var sectionName = $("#selSection option:selected").text();

                var confirm = $mdDialog.confirm()
               .title('Are you sure you want to send all waiting students of Grade:' + $scope.selectedGrade + ' - Section: ' + sectionName + ' to Queue?')
               .ariaLabel('Do you want to proceed?')
               .ok('Yes')
               .cancel('Cancel');
                $mdDialog.show(confirm).then(function () {
                    $scope.isSendAllQueueSubmited = false;
                    $scope.selectedWaitingStudents = $filter('filter')(vm.waitingStudents, { selected: true });
                    if ($scope.selectedWaitingStudents.length > 0) {

                        $scope.hideLoader = false;
                        $scope.progressMode = "indeterminate";

                        $http({
                            method: 'POST',
                            url: basePath + 'Home/SendAllToQueue',
                            data: { StudentList: $scope.selectedWaitingStudents, GateId: JSON.stringify(gateId),sectionId: $scope.selectedSection },
                        }).success(function (data) {
                            angular.forEach($scope.selectedWaitingStudents, function (item) {
                                vm.waitingStudents.splice(vm.waitingStudents.indexOf(item), 1);
                                item.selected = false;
                                vm.queueStudents.splice(0, 0, item);


                            });

                            setNanoScroll();
                            $scope.hideLoader = true;
                            $scope.progressMode = "";
                            if (data == "success") {
                                toasterService.displaySuccessDialog('All students successfully sent to queue');
                            }
                            else{
                                toasterService.displaySuccessDialog("There is no notification sent to guardians of students " + data + " because  guardians for these students are not registered with mobile app");
                            }
                            $scope.chkwaiting = false;
                            $scope.selectedTopGate = "";

                        }).error(function (err) {
                            console.log(err);
                            $scope.hideLoader = true;
                            $scope.progressMode = "";
                        });
                    }
                    else {
                        //toasterService.displayWarningToaster('Plese Select atleast one student!');
                        toasterService.displaySuccessDialog('Plese select atleast one student.')
                    }
                })


            }
        }

        $scope.closeToast = function () {
            $mdToast.hide();
        }

        $scope.sendToDismiss = function (student, index) {
            // console.log(index);
            if (student.selected) {
                $scope.hideLoader = false;
                $scope.progressMode = "indeterminate";
                $http({
                    method: 'post',
                    url: basePath + 'home/SendToDismiss',
                    data: {
                        student: student,sectionId: $scope.selectedSection
                    },
                }).success(function (data) {
                    if (data == "GuardianNotInGeoFence") {
                        toasterService.displaySuccessDialog("Guardian is not in geofence area");
                    }
                    else if (data.Status != 2) {
                        vm.queueStudents.splice(index, 1);
                        vm.dismissStudents.splice(0, 0, data);
                        toasterService.displaySuccessDialog('Student ' + student.FirstName + " successfully sent to dismiss");
                    }
                    setNanoScroll();
                    $scope.hideLoader = true;
                    $scope.progressMode = "";

                }).error(function (err) {
                    console.log(err);
                    $scope.hideLoader = true;
                    $scope.progressMode = "";
                });
            }
            else {
                toasterService.displaySuccessDialog('Plese select student for dismiss');
            }

        }


        $scope.sendAllToDismiss = function () {


            var sectionName = $("#selSection option:selected").text();
            var confirm = $mdDialog.confirm()
                .title('Are you sure you want to Dismiss all students from Grade:' + $scope.selectedGrade + ' - Section: ' + sectionName + ' from Queue?')
           //.title('Do you want to send all queue students  of Grade' + $scope.selectedGrade + ' and section' + $scope.selectedSection + ' to dismiss')
           .ariaLabel('Do you want to proceed?')
           .ok('Yes')
           .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                $scope.hideLoader = false;
                $scope.progressMode = "indeterminate";
                vm.selectedQueueStudents = [];
                $scope.selectedQueueStudents = $filter('filter')(vm.queueStudents, { selected: true });
                if ($scope.selectedQueueStudents.length > 0) {
                    $http({
                        method: 'POST',
                        url: basePath + 'Home/SendAllToDismiss',
                        data: { StudentList: $scope.selectedQueueStudents, sectionId: $scope.selectedSection },
                    }).success(function (data) {

                        if (data == "Success") {

                            angular.forEach($scope.selectedQueueStudents, function (item) {
                                vm.queueStudents.splice(vm.queueStudents.indexOf(item), 1);
                                item.selected = false;
                                vm.dismissStudents.splice(0, 0, item);
                            });
                           
                            toasterService.displaySuccessDialog("All students successfully sent to dismiss");
                          
                        }
                        else {


                            angular.forEach(data.Item1, function (item) {
                               
                                var filteredItem = $filter('filter')(vm.queueStudents,item.Id)[0];
                                vm.queueStudents.splice(vm.queueStudents.indexOf(filteredItem), 1);
                                filteredItem.selected = false;
                                vm.dismissStudents.splice(0, 0, filteredItem);
                            });
                            angular.forEach(vm.queueStudents, function (item) {
                                item.selected = false;
                            });
                            toasterService.displaySuccessDialog(data.Item2+" students are not dismissed bacause guardians are not in geofence." );
                        }

                        setNanoScroll();
                        $scope.hideLoader = true;
                        $scope.progressMode = "";
                        $scope.chkinqueue = false;

                    }).error(function (err) {
                        console.log(err);
                        $scope.hideLoader = true;
                        $scope.progressMode = "";
                        //toasterService.displaySuccessDialog("ToasterSuccessUpdate", "All students successfully sent to dismiss");
                    });
                }
                else {
                    toasterService.displaySuccessDialog('Plese Select atleast one student!');
                }
            })

        }

        $scope.onChange = function (cbState) {
            angular.forEach(vm.waitingStudents, function (item) {
                item.selected = $scope.chkwaiting;
            });
        };

        $scope.onQueueChange = function (checkState) {
            angular.forEach(vm.queueStudents, function (item) {
                item.selected = $scope.chkinqueue;
            });
        };
        $scope.onDismissedChange = function (checkState) {
            angular.forEach(vm.dismissStudents, function (item) {
                item.selected = $scope.chkDismissed;
            });
        };

        $scope.sendBackFromDismissToQueue = function (student, index) {
            if (student.selected) {
                var confirm = $mdDialog.confirm()
                .title('Are you sure you want to move ' + student.FirstName + ' back to Queue')
                .ariaLabel('Do you want to proceed?')
                .ok('Yes')
                .cancel('Cancel');
                $mdDialog.show(confirm).then(function () {
                    $scope.hideLoader = false;
                    $scope.progressMode = "indeterminate";
                    $http({
                        method: 'POST',
                        url: basePath + 'Home/SendBackFromDismissToQueue',
                        data: { student: student, sectionId: $scope.selectedSection },
                    }).success(function (data) {
                        vm.dismissStudents.splice(index, 1);
                        vm.queueStudents.splice(0, 0, data);
                        $scope.hideLoader = true;
                        $scope.progressMode = "";
                        toasterService.displaySuccessDialog('Student ' + student.FirstName + ' successfully sent back to queue');
                        setNanoScroll();

                    }).error(function (err) {
                        $scope.hideLoader = true;
                        $scope.progressMode = "";

                    });
                });
            }
            else {
                toasterService.displaySuccessDialog('Plese select student');
            }

        }
        $scope.sendBackFromQueueToWaiting = function (student, index) {
          if (student.selected) {
             var confirm = $mdDialog.confirm()
            .title('Are you sure you want to move ' + student.FirstName + ' back to Waiting')
            .ariaLabel('Do you want to proceed?')
            .ok('Yes')
            .cancel('Cancel');
                $mdDialog.show(confirm).then(function () {
                    $scope.hideLoader = false;
                    $scope.progressMode = "indeterminate";
                    $http({
                        method: 'POST',
                        url: basePath + 'Home/SendBackFromQueueToWaiting',
                        data: { student: student, sectionId: $scope.selectedSection },
                    }).success(function (data) {
                        vm.queueStudents.splice(index, 1);
                        vm.waitingStudents.splice(0, 0, data);
                        $scope.hideLoader = true;
                        $scope.progressMode = "";
                        toasterService.displaySuccessDialog('Student ' + student.FirstName + ' successfully sent back to waiting');
                        setNanoScroll();

                    }).error(function (err) {
                        $scope.hideLoader = true;
                        $scope.progressMode = "";
                    });
                });
            }
            else {
                toasterService.displaySuccessDialog('Plese select student');
            }

        }
        $scope.sendBackAllFromDismissedToQueue = function (student, index) {
            $scope.selectedDismissedStudents = $filter('filter')(vm.dismissStudents, { selected: true });
            if ($scope.selectedDismissedStudents.length > 0) {
                var confirm = $mdDialog.confirm()
               .title('Are you sure you want to move all students back to Queue')
               .ariaLabel('Do you want to proceed?')
               .ok('Yes')
               .cancel('Cancel');
                $mdDialog.show(confirm).then(function () {
                    $scope.hideLoader = false;
                    $scope.progressMode = "indeterminate";
                    $http({
                        method: 'POST',
                        url: basePath + 'Home/SendBackAllFromDismissedToQueue',
                        data: { StudentList: $scope.selectedDismissedStudents, sectionId: $scope.selectedSection },
                    }).success(function (data) {
                        angular.forEach($scope.selectedDismissedStudents, function (item) {
                            vm.dismissStudents.splice(vm.dismissStudents.indexOf(item), 1);
                            item.selected = false;
                            vm.queueStudents.splice(0, 0, item);
                        });
                        $scope.hideLoader = true;
                        $scope.progressMode = "";
                        toasterService.displaySuccessDialog('All students successfully sent back to queue');
                        $scope.chkDismissed = false;
                        setNanoScroll();


                    }).error(function (err) {
                        $scope.hideLoader = true;
                        $scope.progressMode = "";
                    });

                });
            }
            else{
                toasterService.displaySuccessDialog('Please select atleast one student');
            }
            
          

        }
        $scope.sendBackAllFromQueueToWaiting = function (student, index) {
            $scope.selectedQueueStudents = $filter('filter')(vm.queueStudents, { selected: true });
            if ($scope.selectedQueueStudents.length > 0) {
                var confirm = $mdDialog.confirm()
               .title('Are you sure you want to move all students back to Waiting')
               .ariaLabel('Do you want to proceed?')
               .ok('Yes')
               .cancel('Cancel');
                $mdDialog.show(confirm).then(function () {
                    $scope.hideLoader = false;
                    $scope.progressMode = "indeterminate";

                    $http({
                        method: 'POST',
                        url: basePath + 'Home/SendBackAllFromQueueToWaiting',
                        data: { StudentList: $scope.selectedQueueStudents, sectionId: $scope.selectedSection },
                    }).success(function (data) {
                        angular.forEach($scope.selectedQueueStudents, function (item) {
                            vm.queueStudents.splice(vm.queueStudents.indexOf(item), 1);
                            item.selected = false;
                            vm.waitingStudents.splice(0, 0, item);
                        });
                        $scope.hideLoader = true;
                        $scope.progressMode = "";
                        toasterService.displaySuccessDialog('All students successfully sent back to waiting');
                        $scope.chkinqueue = false;
                        setNanoScroll();

                    }).error(function (err) {
                        $scope.hideLoader = true;
                        $scope.progressMode = "";
                    });

                });
            }
            else {
                toasterService.displaySuccessDialog('Please select atleast one student');
            }


        }


        function setNanoScroll() {
            setTimeout(function () {
                $('.nano').nanoScroller({
                    preventPageScrolling: true,
                    alwaysVisible: true
                });
            }, 100);
        }


        window.MyApp.$rootScope.$on("SignalRUpdateToHome", function (event, notificationList) {
            angular.forEach(notificationList, function (notification, key) {
                if ($scope.selectedSection == notification.Section && $scope.selectedGrade == notification.Grade) {
                    var item = $filter('filter')(vm.dismissStudents, { 'Id': notification.StudentId });
                    var index = vm.dismissStudents.indexOf(item[0]);
                    vm.dismissStudents.splice(index, 1)
                    vm.pickupStudents.splice(0, 0, item[0]);
                    $scope.$apply();

                }
            });
        });
       

    };
})();


