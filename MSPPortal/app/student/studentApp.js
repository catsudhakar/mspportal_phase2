﻿
var studentModule = angular.module('student', ['share'])
    .config(function ($routeProvider, $locationProvider) {
        $routeProvider.when('/students',
            {
                templateUrl: '/app/student/studentList.html'

            });


        $routeProvider.otherwise({ redirectTo: '/students' });
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    });





(function (myApp) {
    var studentsService = function ($rootScope, $http, $q, $location, viewModelHelper) {

        var self = this;

        // self.customerId = 0;

        return this;
    };
    myApp.studentsService = studentsService;
}(window.MyApp));


