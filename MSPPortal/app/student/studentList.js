﻿(function () {
    'use strict';

    angular
        .module('student')
        .controller('studentList', studentList);

    studentList.$inject = ['$scope', '$http', '$mdDialog'];

    function studentList($scope, $http, $mdDialog) {
        $scope.title = 'Students';
        var basePath = "/";
        $scope.grades = [];
        $scope.gates = [];
        $scope.sections = [];

        $scope.students = [];
        $scope.CurrentPage = 0;
        $scope.pageSize = 10;


        activate();

        function activate() {

            $http({
                method: 'POST',
                url: basePath + 'Home/GetData',

            }).success(function (res) {
                if (res !== null) {
                    $scope.grades = res.Grades;
                    $scope.sections = res.Sections;
                    if (res.Grades.length > 0) {
                        $scope.selectedGrade = res.Grades[0].GradeId;
                    }

                }
            }).error(function (err) {
                console.log(err);
            });

            $http({
                method: 'POST',
                url: basePath + 'Student/GetStudentsList',

            }).success(function (res) {
                if (res !== null) {
                    $scope.students = res;
                }
            }).error(function (err) {
                console.log(err);
            });

        }

        $scope.getSectionByGradeId = function (GradeId) {
            $scope.selectedSection = '';
            $scope.sections = [];
            $http({
                method: 'POST',
                url: basePath + 'Home/GetSectionByGradeId',
                data: { Id: JSON.stringify(GradeId) },
            }).success(function (data) {
                $scope.sections = data;
            }).error(function (err) {
                console.log(err);
            });
        }


        $scope.numberOfPages = function () {
            return Math.ceil($scope.students.length / $scope.pageSize);
        }

        $scope.addStudent = function (ev) {


            $mdDialog.show({
                controller: DialogController,
                templateUrl: '/app/student/saveStudent.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true
            })
            .then(function (answer) {
                //activate();

            }, function () {

            });
        };
        function DialogController($scope, $mdDialog) {
            $scope.hide = function () {
                $mdDialog.hide();
            };
            $scope.cancel = function () {
                $mdDialog.cancel();
            };
            $scope.answer = function (answer) {

                $mdDialog.hide(answer);
            };
        }
    }
})();

angular.module('student').filter('startFrom', function () {
    return function (input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});
