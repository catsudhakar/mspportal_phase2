﻿using MSPPortal.Models;
using MSPPortal.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using static MSPPortal.Models.GuardisnViewModels;
using System.Collections.Generic;
using System.Collections;
using MSPPortal.Hubs;
using System.Data.Entity;

namespace MSPPortal.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {

        MSPAdminPortalEntities _context = new MSPAdminPortalEntities();


        public ActionResult Index()
        {

            return View();
        }
        [Authorize(Roles = "Admin")]
        [Route("dashboard")]
        public ActionResult Dashboard()
        {

            return View("~/Views/Home/Index.cshtml");
        }




        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        public JsonResult GetData()
        {

            DataViewModel data = new DataViewModel();
            try
            {
                data.Gates = GetGates();
                data.Grades = GetGrades();
                if (data.Grades.Count > 0)
                    data.Sections = GetSections(data.Grades[0].GradeId);
                return Json(data);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        [HttpPost]
        public ActionResult GetAllStudents()
        {

            var lstPost = new List<StudentViewModel>();
            try
            {

                lstPost = _context.Students.Select(x => new StudentViewModel
                {
                    Id = x.Id,
                    FirstName = x.FirstName,
                    DOB = x.DOB,
                    Grade = x.Grade,
                    LastName = x.LastName,
                    MiddleName = x.MiddleName
                    // Guardian = x.Guardian.FirtName
                }).ToList();
                return Json(lstPost);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //[HttpPost]
        //public ActionResult GetAllStudentsByDriverId(Guid DriverId)
        //{

        //    var lstPost = new List<StudentViewModel>();
        //    try
        //    {

        //        lstPost = _context.Students.Where(x => x.GuardianId == DriverId).Select(x => new StudentViewModel
        //        {
        //            Id = x.Id,
        //            FirstName = x.FirstName,
        //            DOB = x.DOB,
        //            Grade = x.Grade,
        //            LastName = x.LastName,
        //            MiddleName = x.MiddleName,
        //            GuardianName = x.Guardian.FirtName
        //        }).ToList();
        //        return Json(lstPost);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //}

        [HttpPost]
        public ActionResult GetDriver(string PhoneNumber)
        {
            var guardian = new GuardianViewModel();
            try
            {
                guardian = _context.Guardians.Where(x => x.PhoneNumber == PhoneNumber).Select(x => new GuardianViewModel
                {
                    Id = x.Id,
                    FirtName = x.FirtName,
                    LastName = x.LastName,
                    PhoneNumber = x.PhoneNumber,

                }).SingleOrDefault();
                return Json(guardian);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        [HttpPost]
        public JsonResult GetAllGate()
        {

            var lstGate = new List<GateViewModel>();
            try
            {

                lstGate = _context.Gates.Select(x => new GateViewModel
                {
                    GateId = x.GateId,
                    GateName = x.GateName
                }).ToList();
                return Json(lstGate);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        [HttpPost]
        public JsonResult GetAllGrade()
        {

            var lstGrade = new List<GradeViewModel>();
            try
            {

                lstGrade = _context.Grades.Select(x => new GradeViewModel
                {
                    GradeId = x.Id,
                    GradeName = x.ClassName
                }).ToList();
                if (lstGrade.Count > 0)
                {

                    lstGrade.Insert(0, (new GradeViewModel { GradeId = 0, GradeName = "Select All" }));
                }
                return Json(lstGrade);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        [HttpPost]
        public JsonResult GetAllSections()
        {

            var lstSections = new List<SectionViewModel>();
            try
            {

                lstSections = _context.Sections.Select(x => new SectionViewModel
                {
                    SectionId = x.SectionId,
                    SectionName = x.SectionName
                    //GradeName=x.
                }).ToList();
                return Json(lstSections);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<GateViewModel> GetGates()
        {

            var lstGate = new List<GateViewModel>();
            try
            {

                lstGate = _context.Gates.Select(x => new GateViewModel
                {
                    GateId = x.GateId,
                    GateName = x.GateName
                }).ToList();
                return lstGate;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<GradeViewModel> GetGrades()
        {

            var lstGrade = new List<GradeViewModel>();
            try
            {

                lstGrade = _context.Grades.Select(x => new GradeViewModel
                {
                    GradeId = x.Id,
                    GradeName = x.ClassName
                }).ToList();
                return lstGrade;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<SectionViewModel> GetSections(int GradeId)
        {

            var lstSection = new List<SectionViewModel>();
            try
            {

                lstSection = _context.Sections.Where(y => y.ClassId == GradeId).Select(x => new SectionViewModel
                {
                    SectionId = x.SectionId,
                    SectionName = x.SectionName
                }).ToList();
                return lstSection;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        [HttpPost]
        public ActionResult GetSectionByGradeId(string Id)
        {
            int GradeId = 0;
            GradeId = Convert.ToInt32(Id);
            var lstSection = new List<SectionViewModel>();
            try
            {

                lstSection = _context.Sections.Where(y => y.ClassId == GradeId).Select(x => new SectionViewModel
                {
                    SectionId = x.SectionId,
                    SectionName = x.SectionName
                }).ToList();
                return Json(lstSection, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        [HttpPost]
        public ActionResult GetAllSectionByGradeId(string Id)
        {
            int GradeId = 0;
            GradeId = Convert.ToInt32(Id);
            var lstSection = new List<SectionViewModel>();
            try
            {
                if (Id != "0")
                {

                    lstSection = _context.Sections.Where(y => y.ClassId == GradeId).Select(x => new SectionViewModel
                    {
                        SectionId = x.SectionId,
                        SectionName = x.SectionName
                    }).ToList();
                }
                else
                {
                    //lstSection = _context.Sections.Select(x => new SectionViewModel
                    lstSection = _context.Sections.Where(y => y.ClassId == GradeId).Select(x => new SectionViewModel
                    {
                        SectionId = x.SectionId,
                        SectionName = x.SectionName + " - Grade "  + x.ClassId
                    }).ToList();
                }


                lstSection.Insert(0, (new SectionViewModel { SectionId = 0, SectionName = "Select All" }));

                return Json(lstSection, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpPost]
        public ActionResult GetSectionsAndNotificationsByGradeId(string Id)
        {
            int GradeId = 0;
            GradeId = Convert.ToInt32(Id);
            var lstSection = new List<SectionViewModel>();
            var allNotifications = new List<NotificationViewModel>();
            try
            {

                lstSection = _context.Sections.Where(y => y.ClassId == GradeId).Select(x => new SectionViewModel
                {
                    SectionId = x.SectionId,
                    SectionName = x.SectionName
                }).ToList();

                allNotifications = GetNotificationsByGradeId(GradeId);

                Tuple<List<SectionViewModel>, List<NotificationViewModel>> tuple =
                    new Tuple<List<SectionViewModel>, List<NotificationViewModel>>(lstSection, allNotifications);
                return Json(tuple, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        [HttpPost]
        public ActionResult SendToWaiting(string GradeId, string SecId)
        {
            var studentList = new List<StudentViewModel>();
            var waitingStudentList = new List<StudentViewModel>();
            try
            {

                studentList = _context.Students
                                     .Where(y => y.Grade == GradeId && y.SectionId == SecId)
                                     .Select(x => new StudentViewModel
                                     {
                                         Id = x.Id,
                                         FirstName = x.FirstName,
                                         DOB = x.DOB,
                                         Grade = x.Grade,
                                         LastName = x.LastName,
                                         MiddleName = x.MiddleName,
                                         FullName = x.FirstName.Trim() + " " + x.LastName.Trim()
                                     }).ToList();
                waitingStudentList = SaveStudentStatusToDatabase(studentList);
                return Json(waitingStudentList);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private List<StudentViewModel> SaveStudentStatusToDatabase(List<StudentViewModel> studentList)
        {
            var todaydate = DateTime.Now.Date;
            var waitingStudentList = new List<StudentViewModel>();
            foreach (StudentViewModel student in studentList)
            {
                var objStudentStaus = _context.StudentStatus
                    .Where(x => x.StudentId == student.Id && x.CurrentDate == todaydate)
                    .SingleOrDefault();
                if (objStudentStaus != null)
                {
                    if (objStudentStaus.Status == (int)StudentStatus.Waiting)
                    {
                        student.Status = (int)StudentStatus.Waiting;
                        waitingStudentList.Add(student);
                    }
                }
                else
                {
                    var status = new StudentStatu
                    {
                        StudentId = student.Id,
                        CurrentDate = DateTime.Now.Date,
                        WaitingTime = DateTime.Now.TimeOfDay,
                        Status = (int)StudentStatus.Waiting,
                        UpdatedTime = DateTime.Now

                    };
                    _context.StudentStatus.Add(status);
                    _context.SaveChanges();
                    student.Status = (int)StudentStatus.Waiting;
                    waitingStudentList.Add(student);
                }
            }
            return waitingStudentList;
        }


        [HttpPost]
        public ActionResult SendToQueue(StudentViewModel Student, int GateId, string SectionId)
        {
            try
            {
                var todaydate = DateTime.Now.Date;
                Notification notification = new Notification();
                var objStudentStaus = _context.StudentStatus
                       .Where(x => x.StudentId == Student.Id && x.CurrentDate == todaydate)
                       .FirstOrDefault();

                if (objStudentStaus != null)
                {
                    if (objStudentStaus.Status == (int)StudentStatus.Waiting)
                    {

                        objStudentStaus.Status = (int)StudentStatus.InQueue;
                        objStudentStaus.GateId = GateId;

                        objStudentStaus.QueueTime = DateTime.Now.TimeOfDay;
                        objStudentStaus.UpdatedTime = DateTime.Now;
                        _context.SaveChanges();

                        Student.Status = (int)StudentStatus.InQueue;

                        var guardianList = _context.StudentGuardians.Where(x => x.StudentId == Student.Id).ToList();
                        bool isAtleastOneGuardianIsRegisterd = false;
                        foreach (var guardian in guardianList)
                        {
                            var objGurdian = _context.Guardians.FirstOrDefault(x => x.Id == guardian.GuardianId);
                            string queueMessage = Student.FirstName +' '+ Student.LastName + " is 'In Queue' at Gate " + GateId;
                            if (objGurdian != null)
                            {
                                if (objGurdian.DeviceId != null)
                                {
                                    isAtleastOneGuardianIsRegisterd = true;
                                    SendPushNotification(Student.Id, Student.FirstName, GateId, DateTime.Now.TimeOfDay, objGurdian.DeviceId, guardian.GuardianId, Student.Grade, SectionId, queueMessage, "InQueue");
                                }

                            }

                        }
                        if (isAtleastOneGuardianIsRegisterd == false)
                        {
                            Student.IsGuardianNotRegisterd = true;
                        }


                    }

                }
                return Json(Student, JsonRequestBehavior.AllowGet);
            }


            catch (Exception ex)
            {
                throw ex;
            }

        }
        [HttpPost]
        public ActionResult SendBackFromDismissToQueue(StudentViewModel Student, string SectionId)
        {
            try
            {
                var todaydate = DateTime.Now.Date;
                Notification notification = new Notification();
                var objStudentStaus = _context.StudentStatus
                       .Where(x => x.StudentId == Student.Id && x.CurrentDate == todaydate)
                       .FirstOrDefault();

                if (objStudentStaus != null)
                {
                    if (objStudentStaus.Status == (int)StudentStatus.Dismiss)
                    {

                        objStudentStaus.Status = (int)StudentStatus.InQueue;
                        objStudentStaus.QueueTime = DateTime.Now.TimeOfDay;
                        objStudentStaus.DismissTime = null;
                        objStudentStaus.UpdatedTime = DateTime.Now;
                        _context.SaveChanges();

                        Student.Status = (int)StudentStatus.InQueue;

                        var guardianList = _context.StudentGuardians.Where(x => x.StudentId == Student.Id).ToList();
                        foreach (var guardian in guardianList)
                        {
                            var objGurdian = _context.Guardians.FirstOrDefault(x => x.Id == guardian.GuardianId);
                            string queueMessage = Student.FirstName + ' '+ Student.LastName + " is sent back 'In Queue' at Gate " + objStudentStaus.GateId;
                            if (objGurdian != null)
                            {
                                if (objGurdian.DeviceId != null)
                                {
                                    SendPushNotification(Student.Id, Student.FirstName, objStudentStaus.GateId.Value, DateTime.Now.TimeOfDay, objGurdian.DeviceId, objGurdian.Id, Student.Grade, SectionId, queueMessage, "InQueue");
                                }
                            }
                        }

                    }


                }
                return Json(Student, JsonRequestBehavior.AllowGet);
            }


            catch (Exception ex)
            {
                throw ex;
            }

        }
        [HttpPost]
        public ActionResult SendBackFromQueueToWaiting(StudentViewModel Student, string SectionId)
        {
            try
            {
                var todaydate = DateTime.Now.Date;
                Notification notification = new Notification();
                var objStudentStaus = _context.StudentStatus
                       .Where(x => x.StudentId == Student.Id && x.CurrentDate == todaydate)
                       .FirstOrDefault();

                if (objStudentStaus != null)
                {
                    if (objStudentStaus.Status == (int)StudentStatus.InQueue)
                    {

                        objStudentStaus.Status = (int)StudentStatus.Waiting;
                        objStudentStaus.WaitingTime = DateTime.Now.TimeOfDay;
                        objStudentStaus.QueueTime = null;
                        objStudentStaus.UpdatedTime = DateTime.Now;
                        _context.SaveChanges();

                        Student.Status = (int)StudentStatus.Waiting;

                        var guardianList = _context.StudentGuardians.Where(x => x.StudentId == Student.Id).ToList();
                        foreach (var guardian in guardianList)
                        {
                            var objGurdian = _context.Guardians.FirstOrDefault(x => x.Id == guardian.GuardianId);
                            string queueMessage = Student.FirstName + ' ' + Student.LastName + " is sent back to 'Waiting' status";
                            if (objGurdian != null)
                            {
                                if (objGurdian.DeviceId != null)
                                {
                                    SendPushNotification(Student.Id, Student.FirstName, objStudentStaus.GateId.Value, DateTime.Now.TimeOfDay, objGurdian.DeviceId, objGurdian.Id, Student.Grade, SectionId, queueMessage, "Waiting");
                                }

                            }

                        }

                    }
                }
                return Json(Student, JsonRequestBehavior.AllowGet);
            }


            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpPost]
        public ActionResult SendAllToQueue(List<StudentViewModel> StudentList, int GateId, string SectionId)
        {
            try
            {
                var todaydate = DateTime.Now.Date;
                Notification notification = new Notification();
                List<Notification> notificationList = new List<Notification>();
                string studentNamesOfGuardiansNotRegistered = "";
                foreach (StudentViewModel Student in StudentList)
                {
                    var objStudentStaus = _context.StudentStatus
                      .Where(x => x.StudentId == Student.Id && x.CurrentDate == todaydate)
                      .SingleOrDefault();
                    if (objStudentStaus != null)
                    {
                        if (objStudentStaus.Status == (int)StudentStatus.Waiting)
                        {

                            objStudentStaus.Status = (int)StudentStatus.InQueue;
                            objStudentStaus.GateId = GateId;
                            objStudentStaus.QueueTime = DateTime.Now.TimeOfDay;
                            objStudentStaus.UpdatedTime = DateTime.Now;
                            _context.SaveChanges();
                            Student.Status = (int)StudentStatus.InQueue;

                            var guardianList = _context.StudentGuardians.Where(x => x.StudentId == Student.Id).ToList();
                            bool isAtleastOneGuardianIsRegisterd = false;
                            foreach (var guardian in guardianList)
                            {
                                var objGurdian = _context.Guardians.FirstOrDefault(x => x.Id == guardian.GuardianId);
                                string queueMessage = Student.FirstName + ' ' +Student.LastName + " is 'In Queue' at Gate " + GateId;
                                if (objGurdian != null)
                                {
                                    if (objGurdian.DeviceId != null)
                                    {
                                        isAtleastOneGuardianIsRegisterd = true;
                                        SendPushNotification(Student.Id, Student.FirstName, GateId, DateTime.Now.TimeOfDay, objGurdian.DeviceId, objGurdian.Id, Student.Grade, SectionId, queueMessage, "InQueue");
                                    }
                                }
                            }
                            if (isAtleastOneGuardianIsRegisterd == false)
                            {
                                studentNamesOfGuardiansNotRegistered = studentNamesOfGuardiansNotRegistered + "," + Student.FirstName;
                            }
                        }

                    }

                }
                if (studentNamesOfGuardiansNotRegistered != "")
                {
                    return Json(studentNamesOfGuardiansNotRegistered.TrimStart(','));
                }
                return Json("success");
            }


            catch (Exception ex)
            {
                throw ex;
            }

        }
        [HttpPost]
        public ActionResult SendBackAllFromQueueToWaiting(List<StudentViewModel> StudentList, string SectionId)
        {
            try
            {
                var todaydate = DateTime.Now.Date;
                Notification notification = new Notification();
                List<Notification> notificationList = new List<Notification>();
                foreach (StudentViewModel Student in StudentList)
                {
                    var objStudentStaus = _context.StudentStatus
                      .Where(x => x.StudentId == Student.Id && x.CurrentDate == todaydate)
                      .SingleOrDefault();
                    if (objStudentStaus != null)
                    {
                        if (objStudentStaus.Status == (int)StudentStatus.InQueue)
                        {


                            objStudentStaus.Status = (int)StudentStatus.Waiting;
                            objStudentStaus.QueueTime = DateTime.Now.TimeOfDay;
                            objStudentStaus.UpdatedTime = DateTime.Now;
                            _context.SaveChanges();
                            Student.Status = (int)StudentStatus.Waiting;

                            var guardianList = _context.StudentGuardians.Where(x => x.StudentId == Student.Id).ToList();
                            foreach (var guardian in guardianList)
                            {
                                var objGurdian = _context.Guardians.FirstOrDefault(x => x.Id == guardian.GuardianId);
                                string queueMessage = Student.FirstName + ' ' + Student.LastName + " is sent back to 'Waiting' status.";
                                if (objGurdian != null)
                                {
                                    if (objGurdian.DeviceId != null)
                                    {
                                        SendPushNotification(Student.Id, Student.FirstName, objStudentStaus.GateId.Value, DateTime.Now.TimeOfDay, objGurdian.DeviceId, objGurdian.Id, Student.Grade, SectionId, queueMessage, "Waiting");
                                    }
                                }
                            }



                        }

                    }

                }


                return Json("success");
            }


            catch (Exception ex)
            {
                throw ex;
            }

        }
        [HttpPost]
        public ActionResult SendBackAllFromDismissedToQueue(List<StudentViewModel> StudentList, string SectionId)
        {
            try
            {
                var todaydate = DateTime.Now.Date;
                Notification notification = new Notification();
                List<Notification> notificationList = new List<Notification>();
                foreach (StudentViewModel Student in StudentList)
                {
                    var objStudentStaus = _context.StudentStatus
                      .Where(x => x.StudentId == Student.Id && x.CurrentDate == todaydate)
                      .SingleOrDefault();
                    if (objStudentStaus != null)
                    {
                        if (objStudentStaus.Status == (int)StudentStatus.Dismiss)
                        {
                            objStudentStaus.Status = (int)StudentStatus.InQueue;
                            objStudentStaus.QueueTime = DateTime.Now.TimeOfDay;
                            objStudentStaus.UpdatedTime = DateTime.Now;
                            _context.SaveChanges();
                            Student.Status = (int)StudentStatus.InQueue;

                            var guardianList = _context.StudentGuardians.Where(x => x.StudentId == Student.Id).ToList();
                            foreach (var guardian in guardianList)
                            {
                                var objGurdian = _context.Guardians.FirstOrDefault(x => x.Id == guardian.GuardianId);
                                string queueMessage = Student.FirstName+ ' ' +Student.LastName + " is sent back to 'In Queue' at Gate " + objStudentStaus.GateId;
                                if (objGurdian != null)
                                {
                                    if (objGurdian.DeviceId != null)
                                    {
                                        SendPushNotification(Student.Id, Student.FirstName, objStudentStaus.GateId.Value, DateTime.Now.TimeOfDay, objGurdian.DeviceId, objGurdian.Id, Student.Grade, SectionId, queueMessage, "InQueue");
                                    }
                                }
                            }

                        }

                    }

                }
                return Json("success");
            }


            catch (Exception ex)
            {
                throw ex;
            }

        }
        [HttpPost]
        public ActionResult SendToDismiss(StudentViewModel Student, string SectionId)
        {
            try
            {
                var todaydate = DateTime.Now.Date;
                Notification notification = new Notification();
                var nearByGuardianList = _context.GetGuardianInGeofence(Student.Id).ToList();

                if (nearByGuardianList.Count() <= 0)
                {
                    return Json("GuardianNotInGeoFence", JsonRequestBehavior.AllowGet);
                }


                var objStudentStaus = _context.StudentStatus
                       .Where(x => x.StudentId == Student.Id && x.CurrentDate == todaydate)
                       .SingleOrDefault();


                if (objStudentStaus != null)
                {
                    if (objStudentStaus.Status == (int)StudentStatus.InQueue)
                    {

                        objStudentStaus.Status = (int)StudentStatus.Dismiss;
                        objStudentStaus.DismissTime = DateTime.Now.TimeOfDay;
                        objStudentStaus.ReminderTime = DateTime.Now.TimeOfDay;
                        objStudentStaus.UpdatedTime = DateTime.Now;
                        _context.SaveChanges();

                        Student.Status = (int)StudentStatus.Dismiss;

                        var guardianList = _context.StudentGuardians.Where(x => x.StudentId == Student.Id).ToList();
                        foreach (var guardian in nearByGuardianList)
                        {

                            string DissmisMessage = Student.FirstName + ' ' + Student.LastName + " is 'Dismissed' at Gate " + objStudentStaus.GateId;
                            if (guardian.DeviceId != null)
                            {
                                SendPushNotification(Student.Id, Student.FirstName, Convert.ToInt32(objStudentStaus.GateId),
                                 DateTime.Now.TimeOfDay, guardian.DeviceId, guardian.Id, Student.Grade, SectionId, DissmisMessage, "In Dismiss");
                            }


                        }
                    }

                }
                return Json(Student, JsonRequestBehavior.AllowGet);
            }


            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpPost]
        public ActionResult SendAllToDismiss(List<StudentViewModel> StudentList, string SectionId)
        {
            try
            {
                var todaydate = DateTime.Now.Date;
                Notification notification = new Notification();
                List<Notification> notificationList = new List<Notification>();
                List<StudentViewModel> dismissedStudentList = new List<StudentViewModel>();

                string guardiansNotInGeoFence = "";

                foreach (StudentViewModel Student in StudentList)
                {


                    var nearByGuardianList = _context.GetGuardianInGeofence(Student.Id).ToList();

                    if (nearByGuardianList.Count() <= 0)
                    {
                        guardiansNotInGeoFence = guardiansNotInGeoFence + "," + Student.FirstName;
                        continue;
                    }


                    var objStudentStaus = _context.StudentStatus
                             .Where(x => x.StudentId == Student.Id && x.CurrentDate == todaydate)
                             .SingleOrDefault();
                    if (objStudentStaus != null)
                    {
                        if (objStudentStaus.Status == (int)StudentStatus.InQueue)
                        {

                            objStudentStaus.Status = (int)StudentStatus.Dismiss;
                            objStudentStaus.DismissTime = DateTime.Now.TimeOfDay;
                            objStudentStaus.ReminderTime = DateTime.Now.TimeOfDay;
                            objStudentStaus.UpdatedTime = DateTime.Now;
                            _context.SaveChanges();
                            Student.Status = (int)StudentStatus.Dismiss;
                            dismissedStudentList.Add(Student);

                            var guardianList = _context.StudentGuardians.Where(x => x.StudentId == Student.Id).ToList();
                            foreach (var guardian in nearByGuardianList)
                            {

                                string DissmisMessage = Student.FirstName + ' ' + Student.LastName + " is  'Dismissed' at Gate " + objStudentStaus.GateId;
                                if (guardian.DeviceId != null)
                                {
                                    SendPushNotification(Student.Id, Student.FirstName, Convert.ToInt32(objStudentStaus.GateId), DateTime.Now.TimeOfDay, guardian.DeviceId, guardian.Id, Student.Grade, SectionId, DissmisMessage, "In Dismiss");
                                }


                            }

                        }

                    }
                }
                if (guardiansNotInGeoFence != "")
                {
                    Tuple<List<StudentViewModel>, string> obj = new Tuple<List<StudentViewModel>, string>(dismissedStudentList, guardiansNotInGeoFence.TrimStart(','));
                    return Json(obj, JsonRequestBehavior.AllowGet);
                }

                return Json("Success");
            }


            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpPost]
        public void SendReminder()
        {
            var todaydate = DateTime.Now.Date;
            TimeSpan addedTime = new TimeSpan(0, 10, 0);

            List<StatusViewModel> StudentList = new List<StatusViewModel>();

            StudentList = GetDismissStudents();

            Notification notification = new Notification();
            List<Notification> notificationList = new List<Notification>();
            try
            {
                foreach (var Student in StudentList)
                {
                    // if (Student.DismissTime.Value >= Student.DismissTime.Value.Add(addedTime))
                    if (Student.ReminderTime.Value.Add(addedTime) <= DateTime.Now.TimeOfDay)
                    {
                        var reminderCount = _context.Notifications
                            .Where(x => x.StudentId == Student.StudentId
                              && System.Data.Entity.DbFunctions.TruncateTime(x.NotificationDate) == System.Data.Entity.DbFunctions.TruncateTime(todaydate)
                              && x.InStatus == "Reminder")
                            .Count();
                        if (reminderCount < 3)
                        {
                            var nearByGuardianList = _context.GetGuardianInGeofence(Student.StudentId).ToList();

                            var objStudentStaus = _context.StudentStatus
                                            .Where(x => x.StudentId == Student.StudentId && x.CurrentDate == todaydate)
                                            .FirstOrDefault();

                            objStudentStaus.ReminderTime = DateTime.Now.TimeOfDay;
                            objStudentStaus.UpdatedTime = DateTime.Now;
                            _context.SaveChanges();

                            var guardianList = _context.StudentGuardians.Where(x => x.StudentId == Student.StudentId).ToList();
                            foreach (var guardian in nearByGuardianList)
                            {

                                string ReminderMessage = Student.FirstName + " " + Student.LastName + " was  'Dismissed' at Gate " + objStudentStaus.GateId;

                                if (guardian.DeviceId != null)
                                {
                                    SendPushNotification(Student.StudentId, Student.FirstName, Convert.ToInt32(objStudentStaus.GateId), DateTime.Now.TimeOfDay, guardian.DeviceId, guardian.Id, Student.Grade, Student.SectionId, ReminderMessage, "Reminder");
                                }


                            }


                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //[HttpPost]
        //public ActionResult SendReminder1(string GradeId, string SectionId)// public void SendReminder(List<StudentViewModel> StudentList)
        //{
        //    var allNotifications = new List<NotificationViewModel>();
        //    var todaydate = DateTime.Now.Date;
        //    TimeSpan addedTime = new TimeSpan(0, 10, 0);

        //    List<StatusViewModel> StudentList = new List<StatusViewModel>();

        //    StudentList = GetDismissStudents();

        //    Notification notification = new Notification();

        //    List<Notification> notificationList = new List<Notification>();
        //    try
        //    {
        //        foreach (StatusViewModel Student in StudentList)
        //        {
        //            // if (Student.DismissTime.Value >= Student.DismissTime.Value.Add(addedTime))
        //            if (Student.ReminderTime.Value.Add(addedTime) <= DateTime.Now.TimeOfDay)
        //            {
        //                var reminderCount = _context.Notifications
        //                    .Where(x => x.StudentId == Student.StudentId
        //                      && System.Data.Entity.DbFunctions.TruncateTime(x.NotificationDate) == System.Data.Entity.DbFunctions.TruncateTime(todaydate)
        //                      && x.InStatus == "Reminder")
        //                    .Count();
        //                if (reminderCount < 3)
        //                {
        //                    var std = _context.Students.FirstOrDefault(x => x.Id == Student.StudentId);

        //                    var objGurdian = _context.Guardians.FirstOrDefault(x => x.Id == std.GuardianId);

        //                    var objStudentStaus = _context.StudentStatus
        //                                    .Where(x => x.StudentId == Student.StudentId && x.CurrentDate == todaydate)
        //                                    .FirstOrDefault();
        //                    var nearByNotificaion = _context.Notifications
        //                     .Where(x => x.GuardianId == std.GuardianId && DbFunctions.TruncateTime(x.NotificationDate) == todaydate && (x.InStatus == "NearBy" || x.InStatus == "InPremises"))
        //                     .FirstOrDefault();

        //                    objStudentStaus.ReminderTime = DateTime.Now.TimeOfDay;
        //                    objStudentStaus.UpdatedTime = DateTime.Now;
        //                    _context.SaveChanges();
        //                    if (nearByNotificaion != null)
        //                    {
        //                        string ReminderMessage = std.FirstName + " " + std.LastName + " was  dismissed at gate number " + objStudentStaus.GateId;
        //                        SendPushNotification(std.Id, std.FirstName, Convert.ToInt32(objStudentStaus.GateId), DateTime.Now.TimeOfDay, objGurdian.DeviceId, objGurdian.Id, std.Grade, std.SectionId, ReminderMessage, "Reminder");
        //                    }
        //                }
        //            }
        //        }



        //        allNotifications = GetNotificationsList(GradeId, SectionId);


        //        return Json(allNotifications, JsonRequestBehavior.AllowGet);

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return Json(allNotifications, JsonRequestBehavior.AllowGet);
        //}

        public string getSectionName(int GradeId, int SectionId)
        {
            var sectionname = _context.Sections.Where(x => x.ClassId == GradeId && x.SectionId == SectionId).FirstOrDefault();
            return sectionname.SectionName;
        }

        [HttpPost]
        public ActionResult GetStudents(string GradeId, string SecId)
        {
            var studentList = new List<StudentViewModel>();
            var WaitingStudentList = new List<StudentViewModel>();
            var QueueStudentList = new List<StudentViewModel>();
            var PickedStudentList = new List<StudentViewModel>();
            var DismissStudentList = new List<StudentViewModel>();


            try
            {
                var todaydate = DateTime.Now.Date;

                studentList = (from s in _context.Students
                               join ss in _context.StudentStatus on s.Id equals ss.StudentId
                               where ss.CurrentDate == todaydate && s.Grade == GradeId && s.SectionId == SecId

                               select new StudentViewModel
                               {
                                   Id = s.Id,
                                   FirstName = s.FirstName,
                                   DOB = s.DOB,
                                   Grade = s.Grade,
                                   LastName = s.LastName,
                                   MiddleName = s.MiddleName,
                                   Status = (int)ss.Status,
                                   Section = s.SectionId,
                                   UpdatedTime = ss.UpdatedTime,
                                   FullName = s.FirstName.Trim() + " " + s.LastName.Trim()


                               }).OrderByDescending(x => x.UpdatedTime).ToList();


                foreach (StudentViewModel sv in studentList)
                {
                    sv.SectionName = getSectionName(Convert.ToInt16(sv.Grade), Convert.ToInt16(sv.Section));
                }
                WaitingStudentList = studentList.Where(x => x.Status == (int)StudentStatus.Waiting).ToList();
                QueueStudentList = studentList.Where(x => x.Status == (int)StudentStatus.InQueue).ToList();
                PickedStudentList = studentList.Where(x => x.Status == (int)StudentStatus.PickedUp).ToList();
                DismissStudentList = studentList.Where(x => x.Status == (int)StudentStatus.Dismiss).ToList();

                Tuple<List<StudentViewModel>, List<StudentViewModel>, List<StudentViewModel>, List<StudentViewModel>> tuple =
                                 new Tuple<List<StudentViewModel>, List<StudentViewModel>, List<StudentViewModel>, List<StudentViewModel>>
                                 (WaitingStudentList, QueueStudentList, PickedStudentList, DismissStudentList);



                return Json(tuple);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public List<StatusViewModel> GetDismissStudents()
        {
            var studentList = new List<StatusViewModel>();
            var DismissStudentList = new List<StatusViewModel>();


            try
            {
                var todaydate = DateTime.Now.Date;

                studentList = (from s in _context.Students
                               join ss in _context.StudentStatus on s.Id equals ss.StudentId
                               where ss.CurrentDate == todaydate && ss.Status == (int)StudentStatus.Dismiss
                               select new StatusViewModel
                               {
                                   StudentId = s.Id,
                                   DismissTime = ss.DismissTime,
                                   ReminderTime = ss.ReminderTime,
                                   FirstName = s.FirstName,
                                   LastName = s.LastName,
                                   Grade = s.Grade,
                                   SectionId = s.SectionId

                               }).ToList();
                //DismissStudentList = studentList.Where(x => x.Status == (int)StudentStatus.Dismiss).ToList();

                return studentList;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //Send Notification

        public void SendPushNotification(Guid StudentId, string StudentName, int GateId, TimeSpan queueTime,
        string deviceId, Guid GurdaianId, string GradeId, string SectionId, string message, string instatus)
        {

            try
            {
                // need to get from global place
                string applicationID = "AIzaSyCDESZfNmDN7ECrwDCTdfXo6QUl3d4Ikvk";
                string senderId = "603615685495";
                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");


                tRequest.Method = "post";
                tRequest.ContentType = "application/json";
                var data = new
                {
                    to = deviceId,
                    priority = "high",
                    notification = new Hashtable() {
                         { "body", message },
                         { "title", instatus },
                         {"sound","Enabled" },
                         { "force-start", 1 },
                         {"priority", 2 }
                     }
                };

                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));
                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
                tRequest.ContentLength = byteArray.Length;
                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                string str = sResponseFromServer;
                            }
                        }
                    }
                }

                //save Notification in databae

                var NewNotification = new Notification();
                NewNotification.StudentId = StudentId;
                NewNotification.GuardianId = GurdaianId;
                NewNotification.Message = message;
                NewNotification.InStatus = instatus;
                NewNotification.NotificationDate = DateTime.Now;
                NewNotification.SectionId = Convert.ToInt32(SectionId);
                NewNotification.GradeId = Convert.ToInt32(GradeId);
                NewNotification.NotificationFrom = "Portal";
                NewNotification.GateId = GateId;
                NewNotification.IsNotified = false;
                _context.Notifications.Add(NewNotification);
                _context.SaveChanges();


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<NotificationViewModel> GetNotificationsByGradeId(int GradeId)
        {
            var notificationList = new List<NotificationViewModel>();
            var todayDate = DateTime.Now.Date;
            try
            {
                notificationList = _context.Notifications
                        .Where(x => (x.InStatus == "PickedUp" || x.InStatus == "NearBy" || x.InStatus == "InPremises")
                        && System.Data.Entity.DbFunctions.TruncateTime(x.NotificationDate) == System.Data.Entity.DbFunctions.TruncateTime(todayDate) && x.GradeId == GradeId)
                        .OrderByDescending(x => x.Id)
                        .Select(x => new NotificationViewModel
                        {
                            Id = x.Id,
                            Grade = x.GradeId.ToString(),
                            Message = x.Message,
                            Status = x.InStatus
                        }).ToList();



                if (GradeId > 0)
                {
                    notificationList = notificationList.Where(x => x.Grade == GradeId.ToString()).ToList();

                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return notificationList;
        }

        private List<NotificationViewModel> GetNotificationsList(string GradeId, string SectionId)
        {
            var notificationList = new List<NotificationViewModel>();
            try
            {
                var todayDate = DateTime.Now.Date;
                if (GradeId == null || SectionId == null)
                {
                    notificationList = _context.Notifications
                          .Where(x => (x.InStatus == "PickedUp" || x.InStatus == "NearBy" || x.InStatus == "InPremises")
                          && System.Data.Entity.DbFunctions.TruncateTime(x.NotificationDate) == System.Data.Entity.DbFunctions.TruncateTime(todayDate))
                          .OrderByDescending(x => x.Id)
                          .Select(x => new NotificationViewModel
                          {
                              Id = x.Id,
                              Message = x.Message,
                              Status = x.InStatus
                          }).ToList();



                }
                else
                {
                    int Gid = Convert.ToInt32(GradeId);
                    int Sid = Convert.ToInt32(SectionId);
                    notificationList = _context.Notifications
                        .Where(x => x.GradeId == Gid && x.SectionId == Sid && (x.InStatus == "PickedUp" || x.InStatus == "NearBy" || x.InStatus == "InPremises")
                        && System.Data.Entity.DbFunctions.TruncateTime(x.NotificationDate) == System.Data.Entity.DbFunctions.TruncateTime(todayDate))
                        // b.Orders.Where(i => EntityFunctions.TruncateTime(i.OrderFinishDate) == EntityFunctions.TruncateTime(dtBillDate) )
                        .OrderByDescending(x => x.Id)
                        .Select(x => new NotificationViewModel
                        {
                            Id = x.Id,
                            Message = x.Message,
                            Status = x.InStatus
                        }).ToList();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return notificationList;
        }

        [HttpPost]
        public ActionResult GetNotifications(string GradeId, string SectionId)
        {

            var list = new MessageRepository().GetNotifications();
            var allNotifications = new List<NotificationViewModel>();
            try
            {
                allNotifications = GetNotificationsList(GradeId, SectionId);
            }
            catch (Exception ex)
            {
                throw ex;

            }

            return Json(allNotifications, JsonRequestBehavior.AllowGet);


        }
        public string RoleName(string userId)
        {

            var roleName = (from u in _context.AspNetUsers
                            join ur in _context.AspNetUserRoles on u.Id equals ur.UserId
                            join r in _context.AspNetRoles on ur.RoleId equals r.Id
                            where u.Id == userId
                            select r).SingleOrDefault();
            if (roleName != null)
                return roleName.Name;
            else
                return "";


        }

        [HttpPost]
        public ActionResult GetStaff()
        {

            var lstPost = new List<StaffViewModel>();
            try
            {
                string name = User.Identity.Name;
                lstPost = _context.AspNetUsers.Select(x => new StaffViewModel
                {
                    Id = x.Id,
                    Email = x.Email,
                    Name = x.Name,
                    MobileNumber = x.MobileNumber
                    //RoleName= RoleName(x.Id)

                }).ToList();

                foreach (StaffViewModel usr in lstPost)
                {
                    usr.RoleName = RoleName(usr.Id);
                    if (name == usr.Email)
                    {
                        usr.loggedUser = true;
                    }
                    else
                    {
                        usr.loggedUser = false;
                    }
                }
                return Json(lstPost);
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }




    }
}