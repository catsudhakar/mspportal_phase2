﻿
using MSPPortal.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MSPPortal.Controllers
{
    [Authorize]
    public class StudentController : Controller
    {

        MSPAdminPortalEntities _context = new MSPAdminPortalEntities();
        // GET: Student
        [Authorize(Roles = "Admin")]
        [Route("students")]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetStudentsList()
        {

            var lstPost = new List<StudentViewModel>();
            try
            {

                lstPost = (from s in _context.Students
                           join gr in _context.Grades on s.Grade equals gr.Id.ToString()
                           join se in _context.Sections on s.SectionId equals se.SectionId.ToString()
                           select new StudentViewModel
                           {
                               Id = s.Id,
                               FirstName = s.FirstName,
                               LastName = s.LastName,
                               Section = se.SectionName,
                               Grade = gr.ClassName
                           }).ToList();

                return Json(lstPost);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}