﻿using MSPPortal.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity;
using System.Globalization;

namespace MSPPortal.Controllers
{
    [Authorize]
    public class NotificationController : Controller
    {

        MSPAdminPortalEntities _context = new MSPAdminPortalEntities();

        // GET: Notification
        [Route("notificationHistory")]

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetNotification(string GradeId, string SectionId, string Fromdate, string Todate, string Status)
        {
            var NotificationList = new List<NotificationListViewModel>();

            try
            {

                NotificationList = _context.sp_GetAllNotifications()
                    .Select(x => new NotificationListViewModel
                    {
                        StudentName = x.StudentName,
                        GuardianName = x.GuardianName,
                        Status = x.InStatus,
                        Message = x.Message,
                        NotificationDate = x.NotificationDate.Value.Date.ToShortDateString(),
                        NotificationTime = x.NotificationDate.HasValue ? x.NotificationDate.Value.Hour.ToString() + ":" + x.NotificationDate.Value.Minute.ToString() + ":" + x.NotificationDate.Value.Second.ToString() : "",
                        NotificationCurrentDate = x.NotificationDate,
                        GradeName = x.ClassName,
                        SectionName = x.sectionName,
                        SectionId = x.SectionId,
                        GradeId = x.GradeId

                    }).OrderByDescending(x => x.NotificationCurrentDate).ToList();

                if (!string.IsNullOrEmpty(GradeId) && !string.IsNullOrEmpty(SectionId))
                {
                    if (GradeId != "0" && SectionId != "0")
                    {
                        NotificationList = NotificationList.Where(x => x.GradeId == GradeId && x.SectionId == SectionId).ToList();
                    }
                    else if (GradeId != "0" && SectionId == "0")
                    {
                        NotificationList = NotificationList.Where(x => x.GradeId == GradeId).ToList();
                    }


                }
                else if (!string.IsNullOrEmpty(GradeId))
                {
                    NotificationList = NotificationList.Where(x => x.GradeId == GradeId).ToList();
                }

                if (!string.IsNullOrEmpty(Fromdate) && !string.IsNullOrEmpty(Todate))
                {
                    DateTime StartDate = DateTime.ParseExact(Fromdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    DateTime EndDate = DateTime.ParseExact(Todate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    EndDate = EndDate.AddMinutes(1439);
                    NotificationList = NotificationList.Where(x => x.NotificationCurrentDate >= StartDate && x.NotificationCurrentDate <= EndDate).ToList();

                }
                else if (!string.IsNullOrEmpty(Fromdate) && string.IsNullOrEmpty(Todate))
                {
                    DateTime StartDate = DateTime.ParseExact(Fromdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    NotificationList = NotificationList.Where(x => x.NotificationCurrentDate >= StartDate).ToList();

                }

                else if (string.IsNullOrEmpty(Fromdate) && !string.IsNullOrEmpty(Todate))
                {
                    DateTime EndDate = DateTime.ParseExact(Todate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    EndDate = EndDate.AddMinutes(1439);
                    NotificationList = NotificationList.Where(x => x.NotificationCurrentDate <= EndDate).ToList();

                }

                if (!string.IsNullOrEmpty(Status))
                {
                    if (Status != "All")
                    {
                        NotificationList = NotificationList.Where(x => x.Status == Status).ToList();
                    }
                }



            }
            catch (Exception ex)
            {
                throw ex;
            }




            return Json(NotificationList);
        }



    }
}