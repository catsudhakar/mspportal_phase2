﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using MSPPortal.Models;
using System.Net.Mail;
using System.Net;
using System.Data.Entity;
using System.Collections.Generic;

namespace MSPPortal.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;



        MSPAdminPortalEntities _context = new MSPAdminPortalEntities();

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            if (User.IsInRole("Admin"))
                return RedirectToAction("Index", "Home");
            else if (User.IsInRole("Staff"))
                return RedirectToAction("StaffIndex", "Staff");
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            //Require the user to have confirmed email before it can log on
            var user = await UserManager.FindByEmailAsync(model.Email);
            if (user != null)
            {

                //var roles = await _userManager.GetRolesAsync(user);
                if (!await UserManager.IsEmailConfirmedAsync(user.Id))
                {
                    ModelState.AddModelError(string.Empty, "You must have confirmed your email to log in");
                    return View(model);
                }
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                return View(model);
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    //var user = await UserManager.FindByEmailAsync(model.Email);
                    bool isAdmin = await UserManager.IsInRoleAsync(user.Id, "Admin");
                    if (isAdmin)
                        return RedirectToAction("Index", "Home");
                    else
                        return RedirectToAction("StaffIndex", "Staff");

                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }
        [HttpPost]
        public ActionResult Delete(string Id)
        {

            if (Id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = _context.AspNetUsers.Where(x => x.Id == Id).FirstOrDefault();

            _context.AspNetUsers.Remove(model);

            int num = _context.SaveChanges();
            return Json(num);
        }

        [AllowAnonymous]
        public ActionResult Register(string Id = null)
        {

            RegisterViewModel model = new RegisterViewModel();
            List<RoleNames> roleList = new List<RoleNames>();

            if (User.IsInRole("Admin"))
            {
                if (Id != null)
                {

                    //model = _context.AspNetUsers.Where(x => x.Id == Id).Select(x => new RegisterViewModel
                    //{
                    //    Id = x.Id,
                    //    Email = x.Email,
                    //    Name = x.Name,
                    //    MobileNumber = x.MobileNumber

                    //}).FirstOrDefault();

                    model = (from u in _context.AspNetUsers
                             join ur in _context.AspNetUserRoles on u.Id equals ur.UserId
                             where u.Id == Id
                             select new RegisterViewModel
                             {
                                 Id = u.Id,
                                 Email = u.Email,
                                 Name = u.Name,
                                 MobileNumber = u.MobileNumber,
                                 RoleName = ur.RoleId
                             }).FirstOrDefault();



                }

                roleList = _context.AspNetRoles.Select(x => new RoleNames { Id = x.Id, Name = x.Name }).ToList();
                // roleList = _context.AspNetRoles.ToList();
                if (roleList.Count > 0)
                {
                    model.Roles = roleList;
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }


                return View(model);
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }

        }

        //
        // POST: /Account/Register

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.Id == null)
                {

                    var user = new ApplicationUser { UserName = model.Email, Email = model.Email, Name = model.Name, MobileNumber = model.MobileNumber };
                    model.Password = CreateRandomPassword(3) + "Pqcd$";
                    var result = await UserManager.CreateAsync(user, model.Password);
                    if (result.Succeeded)
                    {

                        var roleName = _context.AspNetRoles.Where(x => x.Id == model.RoleName).SingleOrDefault();

                        UserManager.AddToRole(user.Id, roleName.Name);


                        // await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

                        string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                        var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                        // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                        //return RedirectToAction("Index", "Home");

                        var body = "<p>You have added to Misk Schools</p>";
                        //body = body + "<p><b>Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a></b></p>";
                        body = body + "<p><b>" + model.Password + "</b>  is your temporary password.You can change later</p><p>Please  <a href=\"" + callbackUrl + "\">click here</a> to activate your account</p>";
                        var message = new MailMessage();
                        message.To.Add(new MailAddress(model.Email)); //replace with valid value
                        message.Subject = "Observer Registration";
                        message.From = new MailAddress("noreply.lrisf@gmail.com", "Misk Schools");
                        message.Body = body + callbackUrl;
                        message.IsBodyHtml = true;
                        using (var smtp = new SmtpClient())
                        {

                            smtp.Host = "smtp.gmail.com";
                            smtp.EnableSsl = true;
                            NetworkCredential NetworkCred = new NetworkCredential("noreply.lrisf@gmail.com", "Friday$1");
                            smtp.UseDefaultCredentials = true;
                            smtp.Credentials = NetworkCred;
                            smtp.Port = 587;
                            await smtp.SendMailAsync(message);
                            TempData["Message"] = "Observer registered successfully";
                            return RedirectToAction("Index", "Staff");
                        }
                    }

                    AddErrors(result);
                }
                else
                {



                    var users = _context.AspNetUsers.Where(x => x.Id == model.Id).FirstOrDefault();

                    var userRole = _context.AspNetUserRoles.Where(x => x.UserId == model.Id).FirstOrDefault();
                    _context.AspNetUserRoles.Remove(userRole);



                    //var eRoleName = (from r in _context.AspNetRoles
                    //                 join ur in _context.AspNetUserRoles on r.Id equals ur.RoleId
                    //                 where ur.UserId == model.Id
                    //                 select new RegisterViewModel
                    //                 {
                    //                     RoleName = r.Name
                    //                 }).FirstOrDefault();

                    //_userManager.RemoveFromRole(users.Id, eRoleName.RoleName);


                    users.Email = model.Email;
                    users.UserName = model.Email;
                    users.Name = model.Name;
                    users.MobileNumber = model.MobileNumber;

                    _context.SaveChanges();

                    var roleName = _context.AspNetRoles.Where(x => x.Id == model.RoleName).SingleOrDefault();
                    UserManager.AddToRole(users.Id, roleName.Name);

                    _context.SaveChanges();

                    TempData["Message"] = "Observer updated successfully";
                    return RedirectToAction("Index", "Staff");

                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult CheckExistingMobile(string MobileNumber, string InitialMobileNumber)
        {

            bool ifMobileExist = false;

            try

            {
                if (MobileNumber != InitialMobileNumber)
                {
                    var user = _context.AspNetUsers.FirstOrDefault(x => x.MobileNumber == MobileNumber);
                    if (user != null)
                        ifMobileExist = true;
                }

                return Json(!ifMobileExist, JsonRequestBehavior.AllowGet);

            }

            catch (Exception ex)

            {

                return Json(false, JsonRequestBehavior.AllowGet);

            }

        }

        [AllowAnonymous]
        public ActionResult CheckExistingEmail1(string Email)

        {

            bool ifEmailExist = false;

            try

            {

                //ifEmailExist = Email.Equals("mukeshknayak@gmail.com") ? true : false;
                var user = UserManager.FindByEmail(Email);
                if (user != null)
                    ifEmailExist = false;
                else
                    ifEmailExist = true;


                return Json(!ifEmailExist, JsonRequestBehavior.AllowGet);

            }

            catch (Exception ex)

            {

                return Json(false, JsonRequestBehavior.AllowGet);

            }

        }
        //[AllowAnonymous]
        public ActionResult CheckExistingEmail(string Email, string initialEmail)
        {
            bool ifEmailExist = false;
            try
            {
                if (Email != initialEmail)
                {
                    var user = UserManager.FindByEmail(Email);
                    if (user != null)
                        ifEmailExist = true;
                }
                return Json(!ifEmailExist, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            //var user = UserManager.FindById(userId);
            //if (user != null)
            //{
            //    user.EmailConfirmed = true;

            //    _context.SaveChanges();
            //    return View("ConfirmEmail");
            //}
            //return View("Error");

            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }



        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    //ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                    return View("ForgotPasswordConfirmation");
                }

                //For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                //Send an email with this link
                string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                //await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                //return RedirectToAction("ForgotPasswordConfirmation", "Account");


                var body = "<p>You have added to Misk Schools</p>";
                body = body + "<p><b>Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a></b></p>";
                var message = new MailMessage();
                message.To.Add(new MailAddress(model.Email)); //replace with valid value
                message.Subject = "Forgot Password";
                message.From = new MailAddress("noreply.lrisf@gmail.com", "Misk Schools");
                message.Body = body + callbackUrl;
                message.IsBodyHtml = true;
                using (var smtp = new SmtpClient())
                {

                    smtp.Host = "smtp.gmail.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential("noreply.lrisf@gmail.com", "Friday$1");
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    await smtp.SendMailAsync(message);
                    return RedirectToAction("ForgotPasswordConfirmation", "Account");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            if (User.IsInRole("Admin"))
            {
                return RedirectToAction("Dashboard", "Home");
            }
            else
            {
                return RedirectToAction("Index", "Staff");
            }
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        protected string CreateRandomPassword(int passwordLength)
        {

            string numbers = "123456789";
            string otp = string.Empty;
            for (int i = 0; i < passwordLength; i++)
            {
                string character = string.Empty;
                do
                {
                    int index = new Random().Next(0, numbers.Length);
                    character = numbers.ToCharArray()[index].ToString();
                } while (otp.IndexOf(character) != -1);
                otp += character;
            }
            return otp;
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}