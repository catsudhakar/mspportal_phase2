﻿using MSPPortal.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static MSPPortal.Models.GuardisnViewModels;

namespace MSPPortal.Controllers
{
    [Authorize]
    public class StaffController : Controller
    {

        MSPAdminPortalEntities _context = new MSPAdminPortalEntities();

        // GET: Staff
        [Authorize(Roles ="Admin")]
        [Route("staffList")]
        public ActionResult Index()
        {
            if (TempData["Message"] != null)
            {
                ViewBag.SuccessMessage = TempData["Message"];
            }
            return View();
        }
        [Authorize(Roles = "Observer")]
        [Route("staffDashboard")]
        public ActionResult StaffIndex()
        {
             return View("~/Views/Staff/Index.cshtml");
        }

        public string getSectionName(int GradeId, int SectionId)
        {
            var sectionname = _context.Sections.Where(x => x.ClassId == GradeId && x.SectionId == SectionId).FirstOrDefault();
            return sectionname.SectionName;
        }

        [HttpPost]
        public ActionResult GetStudents(string GradeId, string SecId)
        {
            var studentList = new List<StudentViewModel>();
            var waitingStudentList = new List<StudentViewModel>();
            var queueStudentList = new List<StudentViewModel>();
            var pickedStudentList = new List<StudentViewModel>();
            var dismissedStudents = new List<StudentViewModel>();



            try
            {
                var todaydate = DateTime.Now.Date;

               

                    studentList = (from s in _context.Students
                                   join ss in _context.StudentStatus on s.Id equals ss.StudentId
                                   where ss.CurrentDate == todaydate
                                   //&& s.Grade == GradeId && s.SectionId == SecId
                                   select new StudentViewModel
                                   {
                                       Id = s.Id,
                                       FirstName = s.FirstName,
                                       DOB = s.DOB,
                                       Grade = s.Grade,
                                       LastName = s.LastName,
                                       MiddleName = s.MiddleName,
                                       Status = (int)ss.Status,
                                      // GuardianId = s.GuardianId,
                                       Section = s.SectionId,
                                       FullName = s.FirstName.Trim() + " " + s.LastName.Trim()
                                   }).ToList();

                foreach (StudentViewModel sv in studentList)
                {
                    sv.SectionName = getSectionName(Convert.ToInt16(sv.Grade), Convert.ToInt16(sv.Section));
                }

                if (!string.IsNullOrEmpty(GradeId) && !string.IsNullOrEmpty(SecId))
                {
                    studentList = studentList.Where(x => x.Grade == GradeId && x.Section == SecId).ToList();
                }else if(!string.IsNullOrEmpty(GradeId) && GradeId !="0")
                {
                    studentList = studentList.Where(x => x.Grade == GradeId).ToList();
                }

                waitingStudentList = studentList.Where(x => x.Status == (int)StudentStatus.Waiting).ToList();
                queueStudentList = studentList.Where(x => x.Status == (int)StudentStatus.InQueue).ToList();
                dismissedStudents = studentList.Where(x => x.Status == (int)StudentStatus.Dismiss).ToList();
                pickedStudentList = studentList.Where(x => x.Status == (int)StudentStatus.PickedUp).ToList();

                Tuple<List<StudentViewModel>, List<StudentViewModel>,List<StudentViewModel>, List<StudentViewModel>> tuple =
                                 new Tuple<List<StudentViewModel>, List<StudentViewModel>, List<StudentViewModel>, List<StudentViewModel>>
                                 (waitingStudentList, queueStudentList,dismissedStudents, pickedStudentList);



                return Json(tuple);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}